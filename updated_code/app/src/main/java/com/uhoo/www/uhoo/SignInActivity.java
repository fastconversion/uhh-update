package com.uhoo.www.uhoo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import api.Api;
import api.ApiParams;
import parsermodel.User;
import utils.AppSettings;
import utils.CustomRequest;
import utils.NetUtil;
import utils.ProgressUtil;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = SignInActivity.class.getSimpleName();
    
    private static final int REQ_SIGN_UP=100;

    private AppCompatButton btnLogIn,btnSignUp;
    private AppCompatEditText edtEmail,edtPassword;


    private void initialise(){

        edtEmail = (AppCompatEditText)findViewById(R.id.edt_email);
        edtPassword = (AppCompatEditText)findViewById(R.id.edt_password);

        btnLogIn=(AppCompatButton)findViewById(R.id.btn_login);
        btnSignUp= (AppCompatButton)findViewById(R.id.btn_signUp);

        btnLogIn.setOnClickListener(mOnClickListener);
        btnSignUp.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final int id = v.getId();
            switch (id){
                case R.id.btn_login:
                    if(isValidInput()){
                        if(NetUtil.isConnected(SignInActivity.this)) {
                            performLogin();
                        }else{
                            alert(SignInActivity.this,getString(R.string.error_network),getString(R.string.error_network_message));
                        }
                    }
                    break;

                case R.id.btn_signUp:
                    openSignUp();
                    break;
            }
        }
    };

    private boolean isValidInput(){
        boolean  return_value = false;
        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        if(email.isEmpty()
                || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            edtEmail.setError(getString(R.string.invalid_email));
            edtEmail.requestFocus();
        }else if(password.isEmpty()){
            edtPassword.setError(getString(R.string.invalid_password));
            edtPassword.requestFocus();
        }else{
            return_value = true;
        }

        return return_value;
    }



    private void performLogin(){
        ProgressUtil.showProgressDialog(SignInActivity.this, "Sign In", "Signing in please wait.");
        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_USER_NAME,email);
        params.put(ApiParams.KEY_PASSWORD,Api.sha256(password));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL+ Api.LOGIN
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                User user  = new Gson().fromJson(response.toString(),User.class);
                if(user.getStatus() == Api.RESULT_OK){
                    AppSettings.setUser(SignInActivity.this,user);
                    openHome();
                }else{

                    alert(SignInActivity.this
                            , "Sign In Failed"
                            , "Either username or password is invalid please try again.");
                }
                ProgressUtil.dismissDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alert(SignInActivity.this
                        , "Sign In Failed"
                        , "Either username or password is invalid please try again.");

                ProgressUtil.dismissDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(customRequest,Api.LOGIN);
    }

    private   void alert(Context context,String title,String message){

        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    performLogin();
                    dialog.dismiss();
                }
            });
            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Alert dialog crash");
        }
    }

    private void openSignUp(){
        Intent intent = new Intent(SignInActivity.this,SignUpActivity.class);
        startActivityForResult(intent, REQ_SIGN_UP);
    }

    private void openHome(){
        Intent intent = new Intent(SignInActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            System.gc();
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sign_in);
            initialise();
        }catch(Exception e){

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        AppController.getInstance().cancelPendingRequests(Api.LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode){
            case REQ_SIGN_UP:
                if(resultCode == RESULT_OK){
                    finish();
                }
                break;

            default:
                finish();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
