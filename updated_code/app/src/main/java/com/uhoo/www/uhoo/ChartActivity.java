package com.uhoo.www.uhoo;

import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import api.Api;
import api.ApiParams;
import fragments.charts.ChartFragment;
import parsermodel.Day;
import parsermodel.DeviceDetail;
import parsermodel.Hour;
import parsermodel.Month;
import parsermodel.Week;
import utils.AppSettings;
import utils.CustomRequest;
import utils.ProgressUtil;

public class ChartActivity extends AppCompatActivity {

    private static final String TAG = ChartActivity.class.getSimpleName();
    private TabLayout mTabLayout,mTabTime;
    private DeviceDetail mDeviceDetail;
    private AppCompatImageView imgSensor;
    private AppCompatTextView txtValue,txtUnit,txtAvg;
    private ChartFragment mChartFragment;

    private void initialise(){

        mTabLayout=(TabLayout)findViewById(R.id.tabLayout);
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

        mTabTime=(TabLayout)findViewById(R.id.tabTime);
        mTabTime.setOnTabSelectedListener(timeTabSelectedListener);

        imgSensor=(AppCompatImageView)findViewById(R.id.img_sensor);
        txtValue=(AppCompatTextView)findViewById(R.id.txt_value);
        txtUnit=(AppCompatTextView)findViewById(R.id.txt_unit);
        txtAvg=(AppCompatTextView)findViewById(R.id.txt_avg);
    }

    protected void setUI(Fragment fragment){
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container,fragment,fragment.getClass().getCanonicalName());
        mFragmentTransaction.commit();
    }

    private void getDeviceSummary(String serialNumber){
        ProgressUtil.showProgressDialog(ChartActivity.this, "Getting Device Data", "loading device data please wait.");
        String token = AppSettings.getUser(ChartActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_TOKEN, token);
        params.put(ApiParams.KEY_SERIAL_NUMBER,serialNumber);

        Log.e(TAG, "summary params : " + params.toString());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_DEVICE_DATA
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressUtil.dismissDialog();
                Log.e(TAG, "summary : " + response.toString());
                mDeviceDetail = new Gson().fromJson(response.toString(), DeviceDetail.class);
                setUpTab();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(customRequest, serialNumber);

    }

    private void setUpTab(){
        mTabLayout.addTab(mTabLayout.newTab().setText("TEMPERATURE"));
        mTabLayout.addTab(mTabLayout.newTab().setText("HUMIDITY"));
        mTabLayout.addTab(mTabLayout.newTab().setText("CARBON DIOXIDE"));
        mTabLayout.addTab(mTabLayout.newTab().setText("VOC"));
        mTabLayout.addTab(mTabLayout.newTab().setText("PM2.5"));
        mTabLayout.addTab(mTabLayout.newTab().setText("AIR PRESSURE"));
        mTabLayout.addTab(mTabLayout.newTab().setText("CARBON MONOXIDE"));
        mTabLayout.addTab(mTabLayout.newTab().setText("OZONE"));

    }

    private void setTimeTab(){
        mTabTime.addTab(mTabTime.newTab().setText("H"));
        mTabTime.addTab(mTabTime.newTab().setText("D"));
        mTabTime.addTab(mTabTime.newTab().setText("Wk"));
        mTabTime.addTab(mTabTime.newTab().setText("Mo"));
    }

    private  String getSensorName(int pos){
        switch (pos){
            case 0:
                return "temp";
            case 1:
                return "humidity";
            case 2:
                return "co2";
            case 3:
                return "voc";
            case 4:
                return "dust";
            case 5:
                return "pressure";
            case 6:
                return "co";
            case 7:
                return "ozone";

            default:
                return null;
        }
    }

    private TabLayout.OnTabSelectedListener timeTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            int pos = tab.getPosition();
            switch (pos){
                case 0:
                    getHourData("2015-10-13 14:00:00",getSensorName(mTabLayout.getSelectedTabPosition()));
                    break;
                case 1:
                    getDayData("2015-10-13 14:00:00",getSensorName(mTabLayout.getSelectedTabPosition()));
                    break;
                case 2:
                    getWeekData("2015-10-11 12:00:00",getSensorName(mTabLayout.getSelectedTabPosition()));
                    break;
                case 3:
                    getMonthData("2015-10-13 14:00:00",getSensorName(mTabLayout.getSelectedTabPosition()));
                    break;
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void getGraphData(int sensor, int time){

        switch (time){
            case 0:
                getHourData("2015-10-13 14:00:00",getSensorName(sensor));
                break;
            case 1:
                getDayData("2015-10-13 14:00:00",getSensorName(sensor));
                break;
            case 2:
                getWeekData("2015-10-11 12:00:00",getSensorName(sensor));
                break;
            case 3:
                getMonthData("2015-10-13 14:00:00",getSensorName(sensor));
                break;
        }
    }

    private TabLayout.OnTabSelectedListener mOnTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            getGraphData(tab.getPosition(),mTabTime.getSelectedTabPosition());
            int pos = tab.getPosition();
            switch (pos){
                case 0:
                    setSensorValues(mDeviceDetail.getTemp().getValue()
                            , "CELCIUS"
                            , mDeviceDetail.getTemp().getColor()
                            , R.drawable.sensor_temp_white);


                    break;

                case 1:
                    setSensorValues(mDeviceDetail.getHumidity().getValue()
                            , "PERCENT"
                            , mDeviceDetail.getHumidity().getColor()
                            ,R.drawable.sensor_humidity_white);
                    break;

                case 2:
                    setSensorValues(mDeviceDetail.getCo2().getValue()
                            , "PPM"
                            , mDeviceDetail.getCo2().getColor()
                            ,R.drawable.sensor_cdioxide_white);
                    break;

                case 3:
                    setSensorValues(mDeviceDetail.getVoc().getValue()
                            , "PPM"
                            , mDeviceDetail.getVoc().getColor()
                            ,R.drawable.sensor_voc_white);
                    break;

                case 4:
                    setSensorValues(mDeviceDetail.getDust().getValue()
                            ,"ug/m3"
                            ,mDeviceDetail.getDust().getColor()
                            ,R.drawable.sensor_pm25_white);
                    break;

                case 5:
                    setSensorValues(mDeviceDetail.getPressure().getValue()
                            ,"PSI"
                            ,mDeviceDetail.getPressure().getColor()
                            ,R.drawable.sensor_airpressure_white);
                    break;

                case 6:
                    setSensorValues(mDeviceDetail.getCo().getValue()
                            ,"PPM"
                            ,mDeviceDetail.getCo().getColor()
                            ,R.drawable.sensor_cmonoxide_white);
                    break;

                case 7:
                    setSensorValues(mDeviceDetail.getOzone().getValue()
                            ,"PERCENT"
                            ,mDeviceDetail.getOzone().getColor()
                            ,R.drawable.sensor_ozone_white);
                    break;

            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void setSensorValues(double value,String unit,String color,int drawable){
        txtValue.setText(String.valueOf(value) + "\u00B0");
        txtValue.setTextColor(getResources().getColor(getColor(color)));
        txtUnit.setText(unit);
        imgSensor.setBackgroundDrawable(getTintedDrawable(getResources()
                , drawable
                , getColor(color)));

    }


    public Drawable getTintedDrawable(Resources res,
                                      @DrawableRes int drawableResId, @ColorRes int colorResId) {
        Drawable drawable = res.getDrawable(drawableResId);
        int color = res.getColor(colorResId);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    private int getColor(String colorName){
        if(colorName.equals("yellow")){
            return R.color.yellow;
        }else if(colorName.equals("green")){
            return R.color.green;
        }else if(colorName.equals("red")){
            return R.color.red;
        }
        return 0;
    }


    private void getHourData(String dateTime,String sensor){
        ProgressUtil.showProgressDialog(ChartActivity.this,"Please wait","loading chart data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(ChartActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_SERIAL_NUMBER,"56ff76066778574819371567");
        params.put(ApiParams.KEY_PARAM,sensor);
        if(dateTime!=null) {
            params.put(ApiParams.KEY_PREV_DATE_TIME, dateTime);
        }

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_HOUR
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressUtil.dismissDialog();
                Hour hour = new Gson().fromJson(response.toString(),Hour.class);
                mChartFragment.setHourData(hour);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_HOUR);
    }

    private void getDayData(String dateTime,String sensor){
        ProgressUtil.showProgressDialog(ChartActivity.this,"Please wait","loading chart data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(ChartActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_SERIAL_NUMBER,"56ff76066778574819371567");
        params.put(ApiParams.KEY_PARAM,sensor);
        if(dateTime!=null) {
            params.put(ApiParams.KEY_PREV_DATE_TIME, dateTime);
        }

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_DAY
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressUtil.dismissDialog();
                Log.e(TAG,"day "+response.toString());
                Day day = new Gson().fromJson(response.toString(),Day.class);
                mChartFragment.setDayData(day);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_DAY);
    }

    private void getWeekData(String dateTime,String sensor){
        ProgressUtil.showProgressDialog(ChartActivity.this,"Please wait","loading chart data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(ChartActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_SERIAL_NUMBER,"56ff76066778574819371567");
        params.put(ApiParams.KEY_PARAM,sensor);
        if(dateTime!=null) {
            params.put(ApiParams.KEY_PREV_DATE_TIME, dateTime);
        }

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_WEEK
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressUtil.dismissDialog();
                Log.e(TAG,"week "+response.toString());
                Week week = new Gson().fromJson(response.toString(),Week.class);
                mChartFragment.setWeekData(week);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_WEEK);
    }

    private void getMonthData(String dateTime,String sensor){
        ProgressUtil.showProgressDialog(ChartActivity.this,"Please wait","loading chart data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(ChartActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_SERIAL_NUMBER,"56ff76066778574819371567");
        params.put(ApiParams.KEY_PARAM,sensor);
        if(dateTime!=null) {
            params.put(ApiParams.KEY_PREV_DATE_TIME, dateTime);
        }

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_MONTH
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ProgressUtil.dismissDialog();
                Log.e(TAG,"hours "+response.toString());
                Month month = new Gson().fromJson(response.toString(),Month.class);
                mChartFragment.setMonthData(month);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_MONTH);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        initialise();
        mChartFragment = new ChartFragment();
        setUI(mChartFragment);
        getDeviceSummary(getIntent().getStringExtra("serialNumber"));
        setTimeTab();


    }
}
