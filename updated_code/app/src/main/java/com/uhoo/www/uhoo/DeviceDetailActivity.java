package com.uhoo.www.uhoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adapters.DeviceDetailAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.DeviceDetail;
import utils.AppSettings;
import utils.CustomRequest;
import widget.RecyclerView;

public class DeviceDetailActivity extends AppCompatActivity {

    private static final String TAG =DeviceDetailActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private GridLayoutManager mGridLayoutManager;
    private RecyclerView recDeviceDetail;
    private DeviceDetailAdapter mDeviceDetailAdapter;

    private void initialise(){
        mToolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra("deviceName"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGridLayoutManager=new GridLayoutManager(this,2);
        recDeviceDetail=(RecyclerView)findViewById(R.id.recycler_DeviceDetail);
        recDeviceDetail.setLayoutManager(mGridLayoutManager);
    }


    private void getDeviceSummary(String serialNumber){
        String token = AppSettings.getUser(DeviceDetailActivity.this).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_TOKEN, token);
        params.put(ApiParams.KEY_SERIAL_NUMBER,serialNumber);

        Log.e(TAG, "summary params : " + params.toString());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_DEVICE_DATA
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG,"summary : "+response.toString());
                DeviceDetail summary = new Gson().fromJson(response.toString(), DeviceDetail.class);
                mDeviceDetailAdapter = new DeviceDetailAdapter(DeviceDetailActivity.this,summary);
                recDeviceDetail.setAdapter(mDeviceDetailAdapter);
                mDeviceDetailAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(customRequest, serialNumber);

    }

    private void openChartScreen(){
        Intent intent = new Intent(DeviceDetailActivity.this,ChartActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);
        initialise();
        getDeviceSummary(getIntent().getStringExtra("serialNumber"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.device_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id){

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_graph:
                openChartScreen();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
