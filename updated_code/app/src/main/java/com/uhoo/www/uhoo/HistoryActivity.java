package com.uhoo.www.uhoo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import api.ApiParams;
import fragments.home.NotificationHistory;
import parsermodel.Notification;

/**
 * Created by Deepak on 2/5/2016.
 */
public class HistoryActivity extends AppCompatActivity {


  FragmentManager fragmentManager;
  FragmentTransaction fragmentTransaction;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Notification notification=getIntent().getExtras().getParcelable(ApiParams.KEY_NOTIFICATION);
        setContentView(R.layout.activity_notificationhistory);
        fragmentManager=getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();
        Fragment fragment=new NotificationHistory();
        Bundle bundle=new Bundle();
        bundle.putParcelable(ApiParams.KEY_NOTIFICATION,notification);
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.container_body,fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
