package com.uhoo.www.uhoo;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import api.Api;
import api.ApiParams;
import utils.CustomRequest;
import utils.NetUtil;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private AppCompatEditText edtFullName,edtEmail,edtPassword,edtLocation,edtBirthday;
    private RadioButton radioMale,radioFemale;
    private AppCompatButton btnSignUp;
    private Calendar mCalendar;


    private void initialise(){
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.create_account);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtFullName=(AppCompatEditText)findViewById(R.id.edt_full_name);
        edtEmail=(AppCompatEditText)findViewById(R.id.edt_email);
        edtPassword=(AppCompatEditText)findViewById(R.id.edt_password);
        edtLocation=(AppCompatEditText)findViewById(R.id.edt_location);
        edtBirthday=(AppCompatEditText)findViewById(R.id.edt_birth_date);

        radioMale=(RadioButton)findViewById(R.id.radio_male);
        radioFemale=(RadioButton)findViewById(R.id.radio_female);

        btnSignUp=(AppCompatButton)findViewById(R.id.btn_signUp);

        edtBirthday.setOnClickListener(mOnClickListener);
        btnSignUp.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final int id = v.getId();
            switch (id){

                case R.id.edt_birth_date:
                    openDatePicker();
                    break;

                case R.id.btn_signUp:
                    if(isValidInput()){

                        if(NetUtil.isConnected(SignUpActivity.this)) {
                            createAccount();
                        }else{

                            alert(SignUpActivity.this
                                    ,getString(R.string.error_network)
                                    ,getString(R.string.error_network_message));
                        }
                    }
                    break;
            }
        }
    };

    private   void alert(Context context,String title,String message){

        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });


            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Alert dialog crash");
        }
    }



    private void openDatePicker() {
        mCalendar = Calendar.getInstance();
        DatePickerDialog TimePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                edtBirthday.setText(sdf.format(mCalendar.getTime()));
            }
        }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),mCalendar.get(Calendar.DAY_OF_MONTH));
        TimePicker.show();

    }

    private boolean isValidInput(){
        boolean return_value = false;
        String fullName = edtFullName.getText().toString().trim();
        String firstName ="";
        String lastName = "";
        if(fullName.split("\\w+").length>1){

            lastName = fullName.substring(fullName.lastIndexOf(" ")+1);
            firstName = fullName.substring(0, fullName.lastIndexOf(' '));
        }
        else{
            firstName = fullName;
        }

        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String location = edtLocation.getText().toString().trim();
        String birthDay = edtBirthday.getText().toString().trim();

        if(firstName.isEmpty()){
            edtFullName.setError(getString(R.string.invalid_name));
            edtFullName.requestFocus();
        }else if(email.isEmpty()
                ||!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            edtEmail.setError(getString(R.string.invalid_email));
            edtEmail.requestFocus();
        }else if(password.isEmpty()){
            edtPassword.setError(getString(R.string.invalid_password));
            edtPassword.requestFocus();
        }else if(location.isEmpty()){
            edtLocation.setError(getString(R.string.invalid_location));
            edtLocation.requestFocus();
        }else if(birthDay.isEmpty()){
            edtBirthday.setError(getString(R.string.invalid_birthday));
            edtBirthday.requestFocus();
        }else{
            return_value = true;
        }


        return return_value;
    }

    private void createAccount(){

        String fullName = edtFullName.getText().toString().trim();
        String firstName ="";
        String lastName = "";
        if(fullName.split("\\w+").length>1){

            lastName = fullName.substring(fullName.lastIndexOf(" ")+1);
            firstName = fullName.substring(0, fullName.lastIndexOf(' '));
        }
        else{
            firstName = fullName;
        }

        String email = edtEmail.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String location = edtLocation.getText().toString().trim();
        String birthDay = edtBirthday.getText().toString().trim();
        String gender = radioMale.isChecked()? "male":"female";

        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_BIRTH_DAY,birthDay);
        params.put(ApiParams.KEY_GENDER,gender);
        params.put(ApiParams.KEY_GIVEN_NAME,firstName);
        params.put(ApiParams.KEY_LAST_NAME,lastName);
        params.put(ApiParams.KEY_LOCATION,location);
        params.put(ApiParams.KEY_USER_NAME,email);
        params.put(ApiParams.KEY_PASSWORD, Api.sha256(password));

        Log.e(TAG,"params : "+params.toString());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.SIGN_UP
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //TODO :- Need to remove
                Log.e(TAG, "Response " + response.toString());
                Snackbar snackbar= Snackbar.make(btnSignUp,"Account Created",Snackbar.LENGTH_SHORT);
                snackbar.show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(customRequest, Api.LOGIN);

    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialise();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
