package com.uhoo.www.uhoo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.com.ti.cc3x.android.utils.CC3XConstants;
import com.google.gson.Gson;
import com.integrity_project.smartconfiglib.FirstTimeConfig;
import com.integrity_project.smartconfiglib.FirstTimeConfigListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adapters.ViewPagerAdapter;
import api.Api;
import api.ApiParams;
import fragments.DummyFragment;
import fragments.home.HomeFragment;
import fragments.home.NotificationFragment;
import fragments.home.NotificationHistory;
import parsermodel.RandomSalt;
import parsermodel.UncalimedDevice;
import utils.AppSettings;
import utils.CustomRequest;
import utils.CustomViewPager;
import utils.ProgressUtil;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,FirstTimeConfigListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private AppCompatSpinner mNavSpinner;
    private CustomViewPager viewPager;
    private TabLayout tabLayout;
    private FirstTimeConfig firstConfig = null;
    public static ViewPagerAdapter adapter;


    private int[] tabIcons = {R.drawable.toolicon_home
    ,R.drawable.toolicon_events,
    R.drawable.toolicon_notifications
    ,R.drawable.toolicon_settings};

    private int[] tabIconSelected = {R.drawable.toolicon_home_sel
    ,R.drawable.toolicon_events_sel
    ,R.drawable.toolicon_notifications_sel
    ,R.drawable.toolicon_settings_sel};

    private HomeFragment mHomeFragment;
    private NotificationFragment mNotificationFragment;

    private  ViewCallbackListener mViewCallbackListener;
    public boolean isCalled = false;
    private String serverSalt = "";
    private int uhoo_device_value = 1;


    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case CC3XConstants.DLG_CONNECTION_FAILURE:
                    stopPacketData();
                    alert(HomeActivity.this,"","");
                    break;
                case CC3XConstants.DLG_CONNECTION_SUCCESS:
                    stopPacketData();
                    getRandomSalt();
                    break;

                case CC3XConstants.DLG_CONNECTION_TIMEOUT:
                    stopPacketData();
                    alert(HomeActivity.this,"Timeout","Connection has time out.");
                    break;
                case 1000:
                    getSingleRandomSalt();
                    callUmclaimed();
                    break;
            }
            /**
             * Stop transmission
             */
            stopPacketData();
        }
    };





    @Override
    public void onFirstTimeConfigEvent(FtcEvent ftcEvent, Exception e) {

        try{

            e.printStackTrace();
        }catch (Exception e1){
            e1.printStackTrace();
        }
        switch (ftcEvent){

            case FTC_ERROR:
                Log.e(TAG, "FTC ERROR");
                handler.sendEmptyMessage(CC3XConstants.DLG_CONNECTION_FAILURE);
                break;

            case FTC_SUCCESS:
                Log.e(TAG, "FTC SUCCESS");
                handler.sendEmptyMessage(CC3XConstants.DLG_CONNECTION_SUCCESS);
                break;

            case FTC_TIMEOUT:
                handler.sendEmptyMessage(CC3XConstants.DLG_CONNECTION_TIMEOUT);
                Log.e(TAG, "FTC TIMEOUT");

                break;
        }
    }



    public interface ViewCallbackListener{
        public void onViewChanged(int position);
    }

    private void initialise() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        setupViewPager();

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        for(int i = 0;i<tabLayout.getTabCount();i++){
            if(tabLayout.getTabAt(i).isSelected()){
                tabLayout.getTabAt(i).setIcon(tabIconSelected[i]);
            }else{
                tabLayout.getTabAt(i).setIcon(tabIcons[i]);
            }

        }

        tabLayout.setOnTabSelectedListener(mOnTabSelectedListener);


        mNavSpinner = (AppCompatSpinner)findViewById(R.id.spnNavigation);
        mNavSpinner.setOnItemSelectedListener(mOnItemSelectedListener);

    }

    private void openSignIn(){
        Intent intent = new Intent(HomeActivity.this,SignInActivity.class);
        startActivity(intent);
        finish();
    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mHomeFragment = new HomeFragment();
        mNotificationFragment = new NotificationFragment();
        adapter.addFragment(mHomeFragment);
        adapter.addFragment(new DummyFragment());
        adapter.addFragment(new NotificationFragment());
        adapter.addFragment(new DummyFragment());
        viewPager.setAdapter(adapter);
    }


    private TabLayout.OnTabSelectedListener mOnTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            viewPager.setCurrentItem(tab.getPosition());
            if(tab.getPosition()==0){
                mNavSpinner.setVisibility(View.VISIBLE);
            }else{
                mNavSpinner.setVisibility(View.INVISIBLE);
            }
            tab.setIcon(tabIconSelected[tab.getPosition()]);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

            tab.setIcon(tabIcons[tab.getPosition()]);
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    };


    private AdapterView.OnItemSelectedListener mOnItemSelectedListener= new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            if(mViewCallbackListener!=null){
                mViewCallbackListener.onViewChanged(position);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };



    private void openAddDialog(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(HomeActivity.this);

        LayoutInflater layoutInflater = getLayoutInflater();
        View  view = layoutInflater.inflate(R.layout.add_device_dialog, null);
        final AppCompatEditText edtNetworkName = (AppCompatEditText)view.findViewById(R.id.edt_network);
        final AppCompatEditText edtPassword = (AppCompatEditText)view.findViewById(R.id.edt_password);
        final AppCompatSpinner spnUhoos = (AppCompatSpinner)view.findViewById(R.id.spn_uhoos);
        final AppCompatCheckBox showPass = (AppCompatCheckBox)view.findViewById(R.id.showPass);

        showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
                } else {
                    edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        if(AppController.getInstance().getWiFiManagerInstance().getCurrentSSID()!=null
                && AppController.getInstance().getWiFiManagerInstance().getCurrentSSID().length()>0)
        edtNetworkName.setText(AppController.getInstance().getWiFiManagerInstance().getCurrentSSID());

        edtNetworkName.setFocusable(false);
        edtNetworkName.setEnabled(false);
        edtNetworkName.setFocusableInTouchMode(false);



        builder.setView(view);
        builder.setPositiveButton("Start", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String ssid = edtNetworkName.getText().toString();
                String password = edtPassword.getText().toString();
                uhoo_device_value = Integer.valueOf(spnUhoos.getSelectedItem().toString());

                password+=serverSalt;
                try {
                    ProgressUtil.showProgressDialog(HomeActivity.this,"Loading","Loading please wait.");
                    sendPacketData(ssid, password, "");
                    callAddDevice(serverSalt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        builder.show();
    }



    private void sendPacketData(String ssid,String password,String deviceName) throws Exception
    {


            try
            {
                firstConfig = getFirstTimeConfigInstance(ssid,password,deviceName,HomeActivity.this);
            } catch (Exception e)
            {
                e.printStackTrace();
            }

            firstConfig.transmitSettings();

    }



    private FirstTimeConfig getFirstTimeConfigInstance(String ssid,String password,String deviceName,FirstTimeConfigListener apiListener) throws Exception
    {

        if (deviceName.length() == 0)
        {
            deviceName = "CC3000";
        }

        byte[] totalBytes = null;
        String keyConfig = "uhoo_smartconfig";
        totalBytes = keyConfig.getBytes();
        return new FirstTimeConfig(apiListener, password, totalBytes,	AppController.getInstance().getWiFiManagerInstance().getGatewayIpAddress(),
                ssid,deviceName);
    }



    private void stopPacketData()
    {

            try
            {
                ProgressUtil.dismissDialog();
                firstConfig.stopTransmitting();
            } catch (Exception e)
            {
                e.printStackTrace();
            }

    }

    private   void alert(Context context,String title,String message){

        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });


            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Alert dialog crash");
        }
    }

    private void getSingleRandomSalt(){
        ProgressUtil.showProgressDialog(this, "Loading", "Please wait...");
        final String token = "Bearer "+ AppSettings.getUser(this).getToken();
        CustomRequest customRequest = new CustomRequest(Request.Method.GET
                , BuildConfig.BASE_URL + Api.RANDOM_SALT
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG,"salt "+response.toString());
                if(response==null){
                    ProgressUtil.dismissDialog();
                }

                if(response!=null) {
                    RandomSalt randomSalt = new Gson().fromJson(response.toString(), RandomSalt.class);
                    serverSalt = randomSalt.getValue();
                    ProgressUtil.dismissDialog();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"error salt "+error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.RANDOM_SALT);
    }


    private void getRandomSalt(){
        ProgressUtil.showProgressDialog(this, "Loading", "Please wait...");
        final String token = "Bearer "+ AppSettings.getUser(this).getToken();
        CustomRequest customRequest = new CustomRequest(Request.Method.GET
                , Api.BASEURL + Api.RANDOM_SALT
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG,"salt "+response.toString());
                if(response==null){
                    ProgressUtil.dismissDialog();
                }

                    if(response!=null) {
                        RandomSalt randomSalt = new Gson().fromJson(response.toString(), RandomSalt.class);
                        serverSalt = randomSalt.getValue();
                        if(randomSalt.getStatus()==Api.RESULT_OK){
                            callDummy(randomSalt);
                        }else{
                            ProgressUtil.dismissDialog();
                        }

                    }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"error salt "+error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.RANDOM_SALT);
    }

    private void callUmclaimed(){
        final String token = "Bearer "+ AppSettings.getUser(this).getToken();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL + Api.DEVICE_UNCLAIMED
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG,"unclaimed "+response.toString());
                if(response.equals("[]")){
                    Log.e(TAG,"no un-register devices");
                }else{
                    Log.e(TAG, "get un-register devices");
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        int unregisterDevice = jsonArray.length();
                        Intent intent = new Intent(HomeActivity.this,AddNewDevice.class);
                        intent.putExtra("salt",serverSalt);
                        intent.putExtra("devices",unregisterDevice);
                        intent.putExtra("from","homeactivity");
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error dummy " + error.getMessage());

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(ApiParams.KEY_SALT, serverSalt);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_UNCLAIMED);

    }


    private void callDummy(final RandomSalt randomSalt){
        final String token = "Bearer "+ AppSettings.getUser(this).getToken();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL + Api.DEVICE_DUMMY
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG,"dummy "+response.toString());
                if(response==null){
                    ProgressUtil.dismissDialog();
                }else{
                    ProgressUtil.dismissDialog();
                    Intent intent = new Intent(HomeActivity.this,AddNewDevice.class);
                    intent.putExtra("salt",randomSalt.getValue());
                    intent.putExtra("devices",uhoo_device_value);
                    startActivity(intent);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"error dummy "+error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(ApiParams.KEY_SALT, randomSalt.getValue());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_DUMMY);

    }

    private void callAddDevice(String salt){

        ProgressUtil.dismissDialog();
        // remove call dummy1
        Intent intent = new Intent(HomeActivity.this,AddNewDevice.class);
        intent.putExtra("devices",uhoo_device_value);
        intent.putExtra("salt",salt);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        new Thread(new Runnable(){
            public void run(){
                Message msg = new Message();
                msg.what = 1000;
                handler.sendMessage(msg);
            }
        }).start();

        initialise();

        mViewCallbackListener = (ViewCallbackListener)mHomeFragment;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings:
                return true;

            case R.id.action_add:
                if(AppController.getInstance().getWiFiManagerInstance().isWifiConnected()){

                    openAddDialog();
                }else{
                    Toast.makeText(HomeActivity.this, "Please turn on wifi", Toast.LENGTH_SHORT).show();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {

            case R.id.nav_logout:
                AppSettings.logoutUser(HomeActivity.this);
                openSignIn();
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
