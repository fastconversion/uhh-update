package com.uhoo.www.uhoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import api.Api;
import api.ApiParams;
import parsermodel.UncalimedDevice;
import utils.AppSettings;
import utils.ProgressUtil;

public class AddNewDeviceDetailActivity extends AppCompatActivity {

    private static final String TAG = AddNewDeviceDetailActivity.class.getSimpleName();
    private AppCompatButton btnAddDevice;
    private Toolbar mToolbar;
    private AppCompatSpinner spnRoom ,spnFloor;
    private AppCompatEditText edtDeviceName,edtSerialNumber;
    private UncalimedDevice mUncalimedDevice;
    private int devices_value;
    private String devices_salt;

    private void initialise(){
        mUncalimedDevice = new Gson().fromJson(getIntent().getStringExtra("device"),UncalimedDevice.class);
        mToolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("New Device");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtDeviceName=(AppCompatEditText)findViewById(R.id.edt_device_name);
        edtSerialNumber=(AppCompatEditText)findViewById(R.id.edt_serial_number);
        spnFloor=(AppCompatSpinner)findViewById(R.id.spn_floor);
        spnRoom=(AppCompatSpinner)findViewById(R.id.spn_room);

        btnAddDevice =(AppCompatButton)findViewById(R.id.btn_add_device);
        btnAddDevice.setOnClickListener(mOnClickListener);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final int id  = v.getId();
            switch (id){
                case R.id.btn_add_device:
                    if(isValid()){

                        saveDeviceDetail();
                    }
                    break;
            }
        }
    };



    private boolean isValid(){
        boolean result = false;

        String deviceName = edtDeviceName.getText().toString().trim();
        int roon = spnRoom.getSelectedItemPosition();
        int floor = spnFloor.getSelectedItemPosition();
        String serialNumber = edtSerialNumber.getText().toString().trim();

        if(deviceName.isEmpty()){
            edtDeviceName.setError("Please enter device name");
            edtDeviceName.requestFocus();
        }else if(roon==0){
            Toast.makeText(AddNewDeviceDetailActivity.this, "Please select room", Toast.LENGTH_SHORT).show();
        }else if(floor == 0){
            Toast.makeText(AddNewDeviceDetailActivity.this, "Please select floor", Toast.LENGTH_SHORT).show();
        }else if(serialNumber.isEmpty()){
            edtSerialNumber.setError("Please enter serial number");
            edtSerialNumber.requestFocus();
        }else{
            result = true;
        }

        return result;
    }



    private void saveDeviceDetail(){
        ProgressUtil.showProgressDialog(this, "Loading", "Please wait...");

        final String token = "Bearer "+ AppSettings.getUser(this).getToken();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL + Api.SAVE_DEVICE
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "unclaimed " + response);
                if(response==null){
                    ProgressUtil.dismissDialog();
                }else{

                    ProgressUtil.dismissDialog();
                    if(response.contains("1")){
                        setResult(RESULT_OK);
                    }else{
                        Toast.makeText(AddNewDeviceDetailActivity.this,"Error adding device",Toast.LENGTH_SHORT).show();
                    }

                    devices_value = getIntent().getExtras().getInt("devices") -1 ;
                    if(devices_value < 0 )
                        devices_value = 0;

                    devices_salt = getIntent().getExtras().getString("salt");

                    Intent intent = new Intent(AddNewDeviceDetailActivity.this,HomeActivity.class);
                    intent.putExtra("devices",devices_value);
                    intent.putExtra("salt",devices_salt);
                    startActivity(intent);
                    finish();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"error dummy "+error.getMessage());
                ProgressUtil.dismissDialog();
                Toast.makeText(AddNewDeviceDetailActivity.this,"Error adding device",Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("serialNumber",edtSerialNumber.getText().toString());
                params.put("floor",spnFloor.getSelectedItem().toString());
                params.put("roomTypeId", "" + spnRoom.getSelectedItemPosition());
                params.put("deviceName",edtDeviceName.getText().toString());
                Log.e(TAG,"params : "+params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, Api.SAVE_DEVICE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_device_detail);

        initialise();
        edtSerialNumber.setText(mUncalimedDevice.getSerialNumber());

        edtSerialNumber.setFocusable(false);
        edtSerialNumber.setEnabled(false);
        edtSerialNumber.setFocusableInTouchMode(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
