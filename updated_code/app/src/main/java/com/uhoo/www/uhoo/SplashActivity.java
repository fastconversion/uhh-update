package com.uhoo.www.uhoo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

import api.Api;
import utils.AppSettings;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int SPLASH_TIMEOUT = 3000;
    private Timer mTimer;



    //start splash timer
    private void startSplashTimer() {
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {

                if(AppSettings.getUser(SplashActivity.this).getStatus()== Api.RESULT_OK){
                    openHome();
                }else{
                    openSignIn();
                }
            }
        };
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, SPLASH_TIMEOUT);

    }

    private void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    private void openHome(){
        Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void openSignIn(){
        Intent intent = new Intent(SplashActivity.this,SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        cancelTimer();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startSplashTimer();
    }

}
