package com.uhoo.www.uhoo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import adapters.DeviceDetailAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.DeviceDetail;
import utils.AppSettings;
import utils.CustomRequest;
import widget.RecyclerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceDetailFragment extends Fragment {

    private static final String TAG = DeviceDetailFragment.class.getSimpleName();
    private GridLayoutManager mGridLayoutManager;
    private RecyclerView recDeviceDetail;
    private DeviceDetailAdapter mDeviceDetailAdapter;


    private void getDeviceSummary(String serialNumber){
        String token = AppSettings.getUser(getContext()).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_TOKEN,token);
        params.put(ApiParams.KEY_SERIAL_NUMBER,serialNumber);

        Log.e(TAG, "summary params : " + params.toString());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_DEVICE_DATA
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                DeviceDetail summary = new Gson().fromJson(response.toString(), DeviceDetail.class);
                mDeviceDetailAdapter = new DeviceDetailAdapter(getContext(),summary);
                recDeviceDetail.setAdapter(mDeviceDetailAdapter);
                mDeviceDetailAdapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(customRequest, serialNumber);

    }

    public DeviceDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_device_detail, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mGridLayoutManager=new GridLayoutManager(getContext(),2);
        recDeviceDetail=(RecyclerView)view.findViewById(R.id.recycler_DeviceDetail);
        recDeviceDetail.setLayoutManager(mGridLayoutManager);
        getDeviceSummary(getArguments().getString("serialNumber"));
        super.onViewCreated(view, savedInstanceState);
    }
}
