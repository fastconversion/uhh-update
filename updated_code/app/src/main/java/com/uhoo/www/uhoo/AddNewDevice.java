package com.uhoo.www.uhoo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.UnclaimedDeviceAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.UncalimedDevice;
import utils.AppSettings;
import utils.ProgressUtil;
import widget.RecyclerView;

public class AddNewDevice extends AppCompatActivity {

    private static final String TAG = AddNewDevice.class.getSimpleName();
    private static final int DEFAULT_COUNTDOWN_TIME = 60 * 1000; // one minute
    private static final int DEFAULT_INTERVAL = 3000; // 3 second
    private static final int REQ_ADD_DEVICE = 100;
    private AppCompatButton btnContinue;
    private Toolbar mToolbar;
    private List<UncalimedDevice> mUncalimedDevices = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;
    private UnclaimedDeviceAdapter mUnclaimedDeviceAdapter;
    private RecyclerView recUnclaimed;
    private CountDownTimer countDownTimer;
    private int devices_value = 1;
    private String devices_salt = "";
    private String from = "";
    public static AddNewDevice mInstance;
    private int device_count = 0;



    public class MyTimer extends CountDownTimer {


        public MyTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            getDeviceUnclaimed();
        }

        @Override
        public void onFinish() {

            ProgressUtil.dismissDialog();
            if(device_count == 0){
                //back to mainPage
//                countDownTimer.cancel();
                showAlertDialog(devices_value,devices_salt);
            }
        }
    }




    private void initialise(){
        mToolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Add New Device");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLinearLayoutManager = new LinearLayoutManager(this);
        recUnclaimed=(RecyclerView)findViewById(R.id.rec_unclaimed);
        recUnclaimed.setLayoutManager(mLinearLayoutManager);
        mUnclaimedDeviceAdapter=new UnclaimedDeviceAdapter(this,mUncalimedDevices);
        recUnclaimed.setAdapter(mUnclaimedDeviceAdapter);
        recUnclaimed.setEmptyView(findViewById(R.id.empty));
        mUnclaimedDeviceAdapter.setUnclaimedDeviceListener(mUnclaimedDeviceListener);

        if(countDownTimer==null){
            countDownTimer = new MyTimer(DEFAULT_COUNTDOWN_TIME, DEFAULT_INTERVAL);
            countDownTimer.start();
            ProgressUtil.showProgressDialog(this, "Loading", "Please wait...");
        }

        mInstance = this;
    }

    private UnclaimedDeviceAdapter.UnclaimedDeviceListener mUnclaimedDeviceListener = new UnclaimedDeviceAdapter.UnclaimedDeviceListener() {
        @Override
        public void onDeviceSelected(UncalimedDevice uncalimedDevice) {

            String device = new Gson().toJson(uncalimedDevice);
            Intent intent = new Intent(AddNewDevice.this,AddNewDeviceDetailActivity.class);
            intent.putExtra("device",device);
            intent.putExtra("salt",devices_salt);
            startActivityForResult(intent, REQ_ADD_DEVICE);

        }
    };

    private void showAlertDialog(final int devices_value,final String devices_salt) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mInstance);

        alertDialogBuilder.setTitle("uHoo Alert");
        alertDialogBuilder
                .setMessage("No device found. Please try again")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(AddNewDevice.this,HomeActivity.class);
                        intent.putExtra("devices",devices_value);
                        intent.putExtra("salt",devices_salt);
                        startActivity(intent);
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void getDeviceUnclaimed(){

        if(mUncalimedDevices.size() == devices_value){
            ProgressUtil.dismissDialog();
            countDownTimer.cancel();

        }else{
            final String token = "Bearer "+ AppSettings.getUser(this).getToken();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL + Api.DEVICE_UNCLAIMED
                    , new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG,"unclaimed "+response.toString());
                    if((response.equals("[]"))  ){
                        device_count = 0;


                    }else{

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i = 0;i<jsonArray.length();i++){
                                String device = jsonArray.get(i).toString();
                                UncalimedDevice uncalimedDevice = new Gson().fromJson(device,UncalimedDevice.class);
                                if(mUncalimedDevices.size()<= devices_value) {
                                    boolean isSameDevice = false;
                                    for(int j=0;j<mUncalimedDevices.size();j++){
                                        if(mUncalimedDevices.get(j).getSerialNumber().equals(uncalimedDevice.getSerialNumber())) {
                                            isSameDevice = true;
                                        }
                                    }
                                    if(!isSameDevice) {
                                        mUncalimedDevices.add(uncalimedDevice);
                                        device_count = mUncalimedDevices.size();
                                    }

                                }else{
                                    ProgressUtil.dismissDialog();
                                    countDownTimer.cancel();
                                }
                            }

                            mUnclaimedDeviceAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "error dummy " + error.getMessage());

                }
            }){

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put(ApiParams.KEY_SALT, getIntent().getStringExtra("salt"));
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> headers = new HashMap<>();
                    headers.put(ApiParams.KEY_AUTHORIZATION,token);
                    return headers;
                }
            };


            AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_UNCLAIMED);
        }



       /* Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_SALT, getIntent().getStringExtra("salt"));


        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.DEVICE_UNCLAIMED
                , params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "salt " + response.toString());
                if(response==null){
                    ProgressUtil.dismissDialog();
                }else{

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"error salt "+error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };*/


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_device);

        devices_value = getIntent().getExtras().getInt("devices");
        devices_salt = getIntent().getExtras().getString("salt");
        from = getIntent().getExtras().getString("from");
        if(from == null)
            from = "";
        initialise();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ProgressUtil.dismissDialog();
        countDownTimer.cancel();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQ_ADD_DEVICE){
            if(resultCode ==RESULT_OK){
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
