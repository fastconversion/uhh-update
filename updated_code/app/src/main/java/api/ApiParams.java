package api;

/**
 * Created by James on 07-11-2015.
 */
public class ApiParams {
    public static final String KEY_USER_NAME = "username";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_BIRTH_DAY = "birthday";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_GIVEN_NAME="givenName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_TOKEN="token";
    public static final String KEY_LIMIT = "limit";
    public static final String KEY_ORDER = "order";
    public static final String KEY_DATE = "date";
    public static final String KEY_SERIAL_NUMBER = "serialNumber";
    public static final String KEY_AUTHORIZATION="Authorization";
    public static final String KEY_PREV_DATE_TIME ="prevDateTime";
    public static final String KEY_PARAM = "param";
    public static final String KEY_SALT="salt";
    public static final String KEY_NOTIFICATIONS="notifications";
    public static final String KEY_NOTIFICATION="notification";
    public static final String KEY_ID="id";
    public static final String KEY_MESSAGE="message";
    public static final String KEY_LEVEL="level";
    public static final String KEY_COUNTER="counter";
    public static final String KEY_DEVICE_ID="deviceId";
    public static final String KEY_DEVICE_NAME="deviceName";
    public static final String KEY_TYPE_ID="typeId";
    public static final String KEY_UPDATE_AT="updatedAt";
    public static final String KEY_READ="read";



}
