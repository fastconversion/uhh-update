package api;

import android.util.Log;

import com.uhoo.www.uhoo.BuildConfig;

import java.security.MessageDigest;

/**
 * Created by James on 07-11-2015.
 */

public class Api {
    public static String BASEURL="http://api.uhooinc.com/v2/";
    public static final String TAG = Api.class.getSimpleName();
    public static final int RESULT_OK = 1;
    public static final String LOGIN = "user/login";
    public static final String SIGN_UP = "user/add";
    public static final String GET_NOTIFICATION = "notification/get";
    public static final String GET_NOTIFICATON_DETAIL="notification/getdetails";
    public static final String GET_MULTIPLE_NOTIFICATION = "notification/getmultiple";
    public static final String HOME_LIST="notification/next";
    public static final String GET_DEVICE_DATA ="data/get";
    public static final String GET_DEVICES = "device/getdevice";
    public static final String GET_HOUR="data/gethour";
    public static final String GET_DAY="data/getday";
    public static final String GET_WEEK="data/getweek";
    public static final String GET_MONTH="data/getmonth";
    public static final String RANDOM_SALT="device/randomval";
    public static final String DEVICE_DUMMY="device/dummy1";
    public static final String DEVICE_UNCLAIMED="device/unclaimed";
    public static final String SAVE_DEVICE="device/savedetail";


    public static String sha256(String base) {
        String password = BuildConfig.SALT1+base+BuildConfig.SALT2;
        Log.e(TAG,"password : "+password);
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            Log.e(TAG,"sha : "+hexString.toString());
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }




}
