package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by James on 01-01-2016.
 */
public class UncalimedDevice {
    @SerializedName("macAddress")
    @Expose
    private String macAddress;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("firmware")
    @Expose
    private Integer firmware;
    @SerializedName("ccFirmware")
    @Expose
    private Double ccFirmware;
    @SerializedName("salt")
    @Expose
    private String salt;
    @SerializedName("ssid")
    @Expose
    private String ssid;

    /**
     *
     * @return
     * The macAddress
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     *
     * @param macAddress
     * The macAddress
     */
    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     *
     * @return
     * The serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     *
     * @param serialNumber
     * The serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     *
     * @return
     * The firmware
     */
    public Integer getFirmware() {
        return firmware;
    }

    /**
     *
     * @param firmware
     * The firmware
     */
    public void setFirmware(Integer firmware) {
        this.firmware = firmware;
    }

    /**
     *
     * @return
     * The ccFirmware
     */
    public Double getCcFirmware() {
        return ccFirmware;
    }

    /**
     *
     * @param ccFirmware
     * The ccFirmware
     */
    public void setCcFirmware(Double ccFirmware) {
        this.ccFirmware = ccFirmware;
    }

    /**
     *
     * @return
     * The salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     *
     * @param salt
     * The salt
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     *
     * @return
     * The ssid
     */
    public String getSsid() {
        return ssid;
    }

    /**
     *
     * @param ssid
     * The ssid
     */
    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

}


