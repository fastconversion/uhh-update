package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by James on 13-11-2015.
 */
public class Device {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("deviceName")
    @Expose
    private String deviceName;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("lastSyncTime")
    @Expose
    private String lastSyncTime;

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     *
     * @param deviceName
     * The deviceName
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     *
     * @return
     * The serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     *
     * @param serialNumber
     * The serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The lastSyncTime
     */
    public String getLastSyncTime() {
        return lastSyncTime;
    }

    /**
     *
     * @param lastSyncTime
     * The lastSyncTime
     */
    public void setLastSyncTime(String lastSyncTime) {
        this.lastSyncTime = lastSyncTime;
    }

}