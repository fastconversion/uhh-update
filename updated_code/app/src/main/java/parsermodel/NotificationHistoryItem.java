package parsermodel;

/**
 * Created by Deepak on 2/4/2016.
 */
public class NotificationHistoryItem {

    String message;
    int counter,level;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public NotificationHistoryItem(String message, int counter, int level) {

        this.message = message;
        this.counter = counter;
        this.level = level;
    }
}
