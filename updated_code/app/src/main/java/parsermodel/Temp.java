package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by James on 14-11-2015.
 */
public class Temp {

    @SerializedName("value")
    @Expose
    private double value;
    @SerializedName("color")
    @Expose
    private String color;

    /**
     *
     * @return
     * The value
     */
    public double getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

}
