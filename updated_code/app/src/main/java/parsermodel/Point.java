package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by James on 24-11-2015.
 */
public class Point {

    @SerializedName("min")
    @Expose
    private double min;
    @SerializedName("max")
    @Expose
    private double max;

    /**
     *
     * @return
     * The min
     */
    public double getMin() {
        return min;
    }

    /**
     *
     * @param min
     * The min
     */
    public void setMin(double min) {
        this.min = min;
    }

    /**
     *
     * @return
     * The max
     */
    public double getMax() {
        return max;
    }

    /**
     *
     * @param max
     * The max
     */
    public void setMax(double max) {
        this.max = max;
    }

}