package parsermodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by James on 16-11-2015.
 */
public class Notification implements Parcelable {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("level")
    @Expose
    private int  level;
    @SerializedName("counter")
    @Expose
     private Integer counter;

    public Notification(String id, String message, int level, Integer counter, String deviceId, long updatedAt, int typeId, Integer read, String deviceName) {
        this.id = id;
        this.message = message;
        this.level = level;
        this.counter = counter;
        this.deviceId = deviceId;
        this.updatedAt = updatedAt;
        this.typeId = typeId;
        this.read = read;
        this.deviceName = deviceName;
    }

    public Notification(String id, String message, int level, Integer counter, String deviceId, long updatedAt, int typeId, Integer read, String deviceName, String description) {

        this.id = id;
        this.message = message;
        this.level = level;
        this.counter = counter;
        this.deviceId = deviceId;
        this.updatedAt = updatedAt;
        this.typeId = typeId;
        this.read = read;
        this.deviceName = deviceName;
        this.description = description;
    }


    protected Notification(Parcel in) {
        id = in.readString();
        message = in.readString();
        level = in.readInt();
        deviceId = in.readString();
        updatedAt = in.readLong();
        deviceName = in.readString();
        description = in.readString();
        typeId=in.readInt();
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("updatedAt")
    @Expose
    private long updatedAt;
    @SerializedName("typeId")
    @Expose
    private int typeId;
    @SerializedName("read")
    @Expose
    private Integer read;
    @SerializedName("deviceName")
    @Expose
    private String deviceName;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("description")
    @Expose
    private String description;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The level
     */
    public int getLevel() {
        return level;
    }

    /**
     *
     * @param level
     * The level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     *
     * @return
     * The counter
     */
    public Integer getCounter() {
        return counter;
    }

    /**
     *
     * @param counter
     * The counter
     */
    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public long getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updatedAt
     */
    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     *
     * @return
     * The typeId
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     *
     * @param typeId
     * The typeId
     */
    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    /**
     *
     * @return
     * The read
     */
    public Integer getRead() {
        return read;
    }

    /**
     *
     * @param read
     * The read
     */
    public void setRead(Integer read) {
        this.read = read;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(message);
        dest.writeInt(level);
        dest.writeString(deviceId);
        dest.writeLong(updatedAt);
        dest.writeString(deviceName);
        dest.writeString(description);
        dest.writeInt(typeId);
    }
}