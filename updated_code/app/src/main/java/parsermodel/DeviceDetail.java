package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by James on 14-11-2015.
 */
public class DeviceDetail {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("temp")
    @Expose
    private Temp temp;
    @SerializedName("humidity")
    @Expose
    private Humidity humidity;
    @SerializedName("dust")
    @Expose
    private Dust dust;
    @SerializedName("voc")
    @Expose
    private Voc voc;
    @SerializedName("co2")
    @Expose
    private Co2 co2;
    @SerializedName("co")
    @Expose
    private Co co;
    @SerializedName("pressure")
    @Expose
    private Pressure pressure;
    @SerializedName("ethanol")
    @Expose
    private Ethanol ethanol;
    @SerializedName("ozone")
    @Expose
    private Ozone ozone;
    @SerializedName("counter")
    @Expose
    private long counter;
    @SerializedName("timeDiff")
    @Expose
    private long timeDiff;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("status")
    @Expose
    private long status;

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The temp
     */
    public Temp getTemp() {
        return temp;
    }

    /**
     *
     * @param temp
     * The temp
     */
    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    /**
     *
     * @return
     * The humidity
     */
    public Humidity getHumidity() {
        return humidity;
    }

    /**
     *
     * @param humidity
     * The humidity
     */
    public void setHumidity(Humidity humidity) {
        this.humidity = humidity;
    }

    /**
     *
     * @return
     * The dust
     */
    public Dust getDust() {
        return dust;
    }

    /**
     *
     * @param dust
     * The dust
     */
    public void setDust(Dust dust) {
        this.dust = dust;
    }

    /**
     *
     * @return
     * The voc
     */
    public Voc getVoc() {
        return voc;
    }

    /**
     *
     * @param voc
     * The voc
     */
    public void setVoc(Voc voc) {
        this.voc = voc;
    }

    /**
     *
     * @return
     * The co2
     */
    public Co2 getCo2() {
        return co2;
    }

    /**
     *
     * @param co2
     * The co2
     */
    public void setCo2(Co2 co2) {
        this.co2 = co2;
    }

    /**
     *
     * @return
     * The co
     */
    public Co getCo() {
        return co;
    }

    /**
     *
     * @param co
     * The co
     */
    public void setCo(Co co) {
        this.co = co;
    }

    /**
     *
     * @return
     * The pressure
     */
    public Pressure getPressure() {
        return pressure;
    }

    /**
     *
     * @param pressure
     * The pressure
     */
    public void setPressure(Pressure pressure) {
        this.pressure = pressure;
    }

    /**
     *
     * @return
     * The ethanol
     */
    public Ethanol getEthanol() {
        return ethanol;
    }

    /**
     *
     * @param ethanol
     * The ethanol
     */
    public void setEthanol(Ethanol ethanol) {
        this.ethanol = ethanol;
    }

    /**
     *
     * @return
     * The ozone
     */
    public Ozone getOzone() {
        return ozone;
    }

    /**
     *
     * @param ozone
     * The ozone
     */
    public void setOzone(Ozone ozone) {
        this.ozone = ozone;
    }

    /**
     *
     * @return
     * The counter
     */
    public long getCounter() {
        return counter;
    }

    /**
     *
     * @param counter
     * The counter
     */
    public void setCounter(long counter) {
        this.counter = counter;
    }

    /**
     *
     * @return
     * The timeDiff
     */
    public long getTimeDiff() {
        return timeDiff;
    }

    /**
     *
     * @param timeDiff
     * The timeDiff
     */
    public void setTimeDiff(long timeDiff) {
        this.timeDiff = timeDiff;
    }

    /**
     *
     * @return
     * The serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     *
     * @param serialNumber
     * The serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     *
     * @return
     * The status
     */
    public long getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(long status) {
        this.status = status;
    }

}
