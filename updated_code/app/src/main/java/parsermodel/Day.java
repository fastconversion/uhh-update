package parsermodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 02-12-2015.
 */
public class Day {

    @SerializedName("points")
    @Expose
    private List<Double> points = new ArrayList<Double>();
    @SerializedName("low")
    @Expose
    private Double low;
    @SerializedName("high")
    @Expose
    private Double high;
    @SerializedName("ave")
    @Expose
    private Double ave;
    @SerializedName("yhigh")
    @Expose
    private Integer yhigh;
    @SerializedName("ylow")
    @Expose
    private Integer ylow;
    @SerializedName("first")
    @Expose
    private String first;
    @SerializedName("last")
    @Expose
    private String last;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     *
     * @return
     * The points
     */
    public List<Double> getPoints() {
        return points;
    }

    /**
     *
     * @param points
     * The points
     */
    public void setPoints(List<Double> points) {
        this.points = points;
    }

    /**
     *
     * @return
     * The low
     */
    public Double getLow() {
        return low;
    }

    /**
     *
     * @param low
     * The low
     */
    public void setLow(Double low) {
        this.low = low;
    }

    /**
     *
     * @return
     * The high
     */
    public Double getHigh() {
        return high;
    }

    /**
     *
     * @param high
     * The high
     */
    public void setHigh(Double high) {
        this.high = high;
    }

    /**
     *
     * @return
     * The ave
     */
    public Double getAve() {
        return ave;
    }

    /**
     *
     * @param ave
     * The ave
     */
    public void setAve(Double ave) {
        this.ave = ave;
    }

    /**
     *
     * @return
     * The yhigh
     */
    public Integer getYhigh() {
        return yhigh;
    }

    /**
     *
     * @param yhigh
     * The yhigh
     */
    public void setYhigh(Integer yhigh) {
        this.yhigh = yhigh;
    }

    /**
     *
     * @return
     * The ylow
     */
    public Integer getYlow() {
        return ylow;
    }

    /**
     *
     * @param ylow
     * The ylow
     */
    public void setYlow(Integer ylow) {
        this.ylow = ylow;
    }

    /**
     *
     * @return
     * The first
     */
    public String getFirst() {
        return first;
    }

    /**
     *
     * @param first
     * The first
     */
    public void setFirst(String first) {
        this.first = first;
    }

    /**
     *
     * @return
     * The last
     */
    public String getLast() {
        return last;
    }

    /**
     *
     * @param last
     * The last
     */
    public void setLast(String last) {
        this.last = last;
    }

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
