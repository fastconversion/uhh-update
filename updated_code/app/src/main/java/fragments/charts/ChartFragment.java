package fragments.charts;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.uhoo.www.uhoo.R;

import java.util.ArrayList;

import parsermodel.Day;
import parsermodel.Hour;
import parsermodel.Month;
import parsermodel.Week;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment {

    private static final String TAG = ChartFragment.class.getSimpleName();
    private LineChart mChart;

    public void setHourData(Hour hourData){
        mChart.invalidate();


        String firstData = hourData.getFirst();
        int hour = Integer.valueOf(firstData.substring(0,2));
        int min = Integer.valueOf(firstData.substring(3));
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < hourData.getPoints().size(); i++) {
            xVals.add(""+hour+":"+String.format("%02d", (min+i)));
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < hourData.getPoints().size(); i++) {
            yVals.add(new Entry(Float.valueOf(hourData.getPoints().get(i).toString()), i));
        }
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

        set1.setLineWidth(4f);
        set1.setCircleSize(10f);
        set1.setCircleColor(Color.WHITE);
        set1.setCircleColorHole(getResources().getColor(R.color.green));
        set1.setColor(getResources().getColor(R.color.green));
        set1.setFillColor(getResources().getColor(R.color.green));

        // create a data object with the datasets
        LineData data = new LineData(xVals, set1);
        mChart.setData(data);
        for (DataSet<?> set : mChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        mChart.setVisibleXRangeMaximum(5);

        YAxis leftY = mChart.getAxisLeft();
        leftY.setStartAtZero(false);


        YAxis yAxis = mChart.getAxisRight();
        yAxis.setEnabled(false);

        mChart.invalidate();
    }

    public void setDayData(Day dayData){
        mChart.invalidate();


        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < dayData.getPoints().size(); i++) {
            xVals.add(String.format("%02d", (i)));
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < dayData.getPoints().size(); i++) {
            yVals.add(new Entry(Float.valueOf(dayData.getPoints().get(i).toString()), i));
        }
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

        set1.setLineWidth(4f);
        set1.setCircleSize(10f);
        set1.setCircleColor(Color.WHITE);
        set1.setCircleColorHole(getResources().getColor(R.color.green));
        set1.setColor(getResources().getColor(R.color.green));
        set1.setFillColor(getResources().getColor(R.color.green));

        // create a data object with the datasets
        LineData data = new LineData(xVals, set1);
        mChart.setData(data);
        for (DataSet<?> set : mChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        mChart.setVisibleXRangeMaximum(10);

        YAxis leftY = mChart.getAxisLeft();
        leftY.setStartAtZero(false);


        YAxis yAxis = mChart.getAxisRight();
        yAxis.setEnabled(false);

        mChart.invalidate();
    }

    public void setWeekData(Week weekData){

        mChart.invalidate();

        String day = weekData.getFirst();
        int curday = Integer.valueOf(day.substring(0,2));
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < weekData.getPoints().size(); i++) {
            xVals.add(String.format("%02d", (curday+i)));
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < weekData.getPoints().size(); i++) {
            String value = String.valueOf(weekData.getPoints().get(i).getMax());
            yVals.add(new Entry(Float.valueOf(value), i));
        }
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

        set1.setLineWidth(4f);
        set1.setCircleSize(10f);
        set1.setCircleColor(Color.WHITE);
        set1.setCircleColorHole(getResources().getColor(R.color.green));
        set1.setColor(getResources().getColor(R.color.green));
        set1.setFillColor(getResources().getColor(R.color.green));



        ArrayList<Entry> yVals2 = new ArrayList<Entry>();

        for (int i = 0; i < weekData.getPoints().size(); i++) {
            String value = String.valueOf(weekData.getPoints().get(i).getMin());
            yVals2.add(new Entry(Float.valueOf(value), i));
        }
        LineDataSet set2 = new LineDataSet(yVals2, "DataSet 2");

        set2.setLineWidth(4f);
        set2.setCircleSize(10f);
        set2.setCircleColor(Color.WHITE);
        set2.setCircleColorHole(getResources().getColor(R.color.green));
        set2.setColor(getResources().getColor(R.color.green));
        set2.setFillColor(getResources().getColor(R.color.green));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set2);
        dataSets.add(set1);

        LineData data = new LineData(xVals, dataSets);

        mChart.setData(data);
        for (DataSet<?> set : mChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        mChart.setVisibleXRangeMaximum(7);

        YAxis leftY = mChart.getAxisLeft();
        leftY.setStartAtZero(false);


        YAxis yAxis = mChart.getAxisRight();
        yAxis.setEnabled(false);

        mChart.invalidate();
    }

    public void setMonthData(Month monthData){

        mChart.invalidate();

        String day = monthData.getFirst();
        int curday = Integer.valueOf(day.length()==5 ?day.substring(0,2): day.substring(0,1));
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < monthData.getPoints().size(); i++) {
            xVals.add(String.format("%02d", (curday+i)));
        }

        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < monthData.getPoints().size(); i++) {
            String value = String.valueOf(monthData.getPoints().get(i).getMax());
            yVals.add(new Entry(Float.valueOf(value), i));
        }
        LineDataSet set1 = new LineDataSet(yVals, "DataSet 1");

        set1.setLineWidth(4f);
        set1.setCircleSize(10f);
        set1.setCircleColor(Color.WHITE);
        set1.setCircleColorHole(getResources().getColor(R.color.green));
        set1.setColor(getResources().getColor(R.color.green));
        set1.setFillColor(getResources().getColor(R.color.green));



        ArrayList<Entry> yVals2 = new ArrayList<Entry>();

        for (int i = 0; i < monthData.getPoints().size(); i++) {
            String value = String.valueOf(monthData.getPoints().get(i).getMin());
            yVals2.add(new Entry(Float.valueOf(value), i));
        }
        LineDataSet set2 = new LineDataSet(yVals2, "DataSet 2");

        set2.setLineWidth(4f);
        set2.setCircleSize(10f);
        set2.setCircleColor(Color.WHITE);
        set2.setCircleColorHole(getResources().getColor(R.color.green));
        set2.setColor(getResources().getColor(R.color.green));
        set2.setFillColor(getResources().getColor(R.color.green));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set2);
        dataSets.add(set1);

        LineData data = new LineData(xVals, dataSets);

        mChart.setData(data);
        for (DataSet<?> set : mChart.getData().getDataSets())
            set.setDrawValues(!set.isDrawValuesEnabled());
        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        mChart.setVisibleXRangeMaximum(7);

        YAxis leftY = mChart.getAxisLeft();
        leftY.setStartAtZero(false);


        YAxis yAxis = mChart.getAxisRight();
        yAxis.setEnabled(false);

        mChart.invalidate();
    }

    public ChartFragment() {
        // Required empty public constructor
    }

    public static ChartFragment newInstance(){
        return new ChartFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mChart = (LineChart) view.findViewById(R.id.lineChart);
        mChart.setDrawGridBackground(true);
        super.onViewCreated(view, savedInstanceState);
    }
}
