package fragments.home;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.HomeActivity;
import com.uhoo.www.uhoo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeActivity.ViewCallbackListener {

    private static final String TAG = HomeFragment.class.getSimpleName();

    protected void setUI(Fragment fragment){
        FragmentManager mFragmentManager = getChildFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container,fragment,fragment.getClass().getCanonicalName());
        mFragmentTransaction.commit();
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewChanged(int position) {
        Log.e(TAG,"nav position : "+position);

        switch (position){

            case 0: //card view
                setUI(new HomeCardViewFragment());
                break;

            case 1://list view
                setUI(new HomeListViewFragment());
                break;

            case 2: //summary view
                setUI(new HomeSummaryViewFragment());
                break;
        }
    }
}
