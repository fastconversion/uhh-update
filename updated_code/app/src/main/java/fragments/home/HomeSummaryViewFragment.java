package fragments.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.uhoo.www.uhoo.AppController;
import com.uhoo.www.uhoo.BuildConfig;
import com.uhoo.www.uhoo.DeviceDetailActivity;
import com.uhoo.www.uhoo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.SummaryAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.Device;
import parsermodel.DeviceDetail;
import utils.AppSettings;
import utils.CustomRequest;
import utils.ProgressUtil;
import widget.RecyclerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeSummaryViewFragment extends Fragment {

    private static final String TAG = HomeSummaryViewFragment.class.getSimpleName();
    private LinearLayoutManager mLinearLayoutManager;
    private SummaryAdapter mSummaryAdapter;
    private RecyclerView recSummary;
    private List<Device> mDevices;
    private List<DeviceDetail> mSummaries;



    private void getAllDevices(){
        ProgressUtil.showProgressDialog(getContext(), "Getting Device Data", "loading device data please wait.");
        final String token = "Bearer "+AppSettings.getUser(getContext()).getToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET
                , BuildConfig.BASE_URL + Api.GET_DEVICES
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    mDevices = new ArrayList<>();
                    mSummaries = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Device device = new Gson().fromJson(jsonObject.toString(),Device.class);
                        getDeviceSummary(device);
                        mDevices.add(device);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ProgressUtil.dismissDialog();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest,Api.GET_DEVICES);
    }

    private void getDeviceSummary(Device device){
        String token = AppSettings.getUser(getContext()).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_TOKEN,token);
        params.put(ApiParams.KEY_SERIAL_NUMBER,device.getSerialNumber());

        Log.e(TAG, "summary params : " + params.toString());

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_DEVICE_DATA
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                ProgressUtil.dismissDialog();
                DeviceDetail summary = new Gson().fromJson(response.toString(), DeviceDetail.class);
                mSummaries.add(summary);
                if(mDevices.size()==mSummaries.size()){
                    mSummaryAdapter = new SummaryAdapter(getContext(),mDevices,mSummaries);
                    recSummary.setAdapter(mSummaryAdapter);
                    mSummaryAdapter.setSummaryItemClickListener(mSummaryItemClickListener);
                    mSummaryAdapter.notifyDataSetChanged();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(customRequest, device.getSerialNumber());

    }



    private SummaryAdapter.SummaryItemClickListener mSummaryItemClickListener=new SummaryAdapter.SummaryItemClickListener() {
        @Override
        public void onSummaryItemClicked(Device device) {

            Intent intent = new Intent(getActivity(), DeviceDetailActivity.class);
            intent.putExtra("serialNumber",device.getSerialNumber());
            intent.putExtra("deviceName",device.getDeviceName());
            getActivity().startActivity(intent);

        }
    };



    public HomeSummaryViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_summary_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mLinearLayoutManager = new LinearLayoutManager(getContext());

        recSummary=(RecyclerView)view.findViewById(R.id.recycler_Summary);
        recSummary.setLayoutManager(mLinearLayoutManager);

        getAllDevices();
        super.onViewCreated(view, savedInstanceState);
    }
}
