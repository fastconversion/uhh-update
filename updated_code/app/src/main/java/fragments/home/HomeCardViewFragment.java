package fragments.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.uhoo.www.uhoo.AppController;
import com.uhoo.www.uhoo.BuildConfig;
import com.uhoo.www.uhoo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import api.Api;
import api.ApiParams;
import parsermodel.Device;
import parsermodel.DeviceNotification;
import utils.AppSettings;
import utils.CustomRequest;
import utils.CustomViewPager;
import utils.ProgressUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeCardViewFragment extends Fragment {

    private static final String TAG = HomeCardViewFragment.class.getSimpleName();
    private List<Device> mDevices;
    private List<DeviceNotification> mDeviceNotifications;
    private CustomViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;

    public class ViewPagerAdapter extends FragmentPagerAdapter{


        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new CardViewItemFragment(mDevices.get(position),mDeviceNotifications.get(position).getNotifications());
        }

        @Override
        public int getCount() {
            return mDevices.size();
        }
    }

    private void getAllDevices(){

        ProgressUtil.showProgressDialog(getContext(),"Getting Device Data","loading device data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(getContext()).getToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET
                , BuildConfig.BASE_URL + Api.GET_DEVICES
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    mDevices = new ArrayList<>();
                    mDeviceNotifications=new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Device device = new Gson().fromJson(jsonObject.toString(),Device.class);
                        mDevices.add(device);
                    }
                    getNotifications();

                } catch (JSONException e) {
                    e.printStackTrace();
                    ProgressUtil.dismissDialog();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, Api.GET_DEVICES);
    }


    private void getNotifications(){
        final String token = "Bearer "+ AppSettings.getUser(getContext()).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_LIMIT, String.valueOf(5));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_MULTIPLE_NOTIFICATION
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ProgressUtil.dismissDialog();
                try {
                for(Device device: mDevices){
                    if(response.has(device.getSerialNumber())){

                            DeviceNotification deviceNotification = new Gson().fromJson(response.getJSONObject(device.getSerialNumber()).toString()
                            ,DeviceNotification.class);
                        mDeviceNotifications.add(deviceNotification);

                    }
                }
                    try{
                        if(mDeviceNotifications.size()==mDevices.size()){
                            mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
                            mViewPager.setAdapter(mViewPagerAdapter);
                        }
                    }catch(Exception e){

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    ProgressUtil.dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_MULTIPLE_NOTIFICATION);

    }

    public HomeCardViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_card_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager=(CustomViewPager)view.findViewById(R.id.viewpager);
        getAllDevices();
        super.onViewCreated(view, savedInstanceState);
    }
}
