package fragments.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.uhoo.www.uhoo.AppController;
import com.uhoo.www.uhoo.BuildConfig;
import com.uhoo.www.uhoo.DeviceDetailActivity;
import com.uhoo.www.uhoo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.ListAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.Device;
import parsermodel.DeviceNotification;
import parsermodel.Notification;
import utils.AppSettings;
import utils.CustomRequest;
import utils.MyLinearLayoutManager;
import utils.ProgressUtil;
import widget.RecyclerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeListViewFragment extends Fragment {

    private static final String TAG = HomeListViewFragment.class.getSimpleName();
    private List<Device> mDevices;
    private List<DeviceNotification> mDeviceNotifications;
    private ViewGroup mContainer;


    private void addCard(final Device device, List<Notification> notifications){
        final MyLinearLayoutManager linearLayoutManager = new MyLinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        final ViewGroup notificationCard = (ViewGroup) LayoutInflater.from(getContext()).inflate(
                R.layout.card_view_item, mContainer, false);
        final AppCompatTextView txtTitle = (AppCompatTextView)notificationCard.findViewById(R.id.txt_device_name);
        txtTitle.setText(device.getDeviceName());
        final RecyclerView recyclerView = (RecyclerView)notificationCard.findViewById(R.id.recycler_Card);
        recyclerView.setLayoutManager(linearLayoutManager);
        final ListAdapter listAdapter = new ListAdapter(getContext(),notifications);
        recyclerView.setAdapter(listAdapter);
        notificationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DeviceDetailActivity.class);
                intent.putExtra("serialNumber",device.getSerialNumber());
                intent.putExtra("deviceName",device.getDeviceName());
                getActivity().startActivity(intent);
            }
        });
        mContainer.addView(notificationCard);
    }

    private void getAllDevices(){
        ProgressUtil.showProgressDialog(getContext(), "Getting Device Data", "loading device data please wait.");
        final String token = "Bearer "+ AppSettings.getUser(getContext()).getToken();

        StringRequest stringRequest = new StringRequest(Request.Method.GET
                , BuildConfig.BASE_URL + Api.GET_DEVICES
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    mDevices = new ArrayList<>();
                    mDeviceNotifications = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    for(int i = 0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Device device = new Gson().fromJson(jsonObject.toString(),Device.class);
                        mDevices.add(device);
                    }

                    getNotifications();
                } catch (JSONException e) {
                    e.printStackTrace();
                    ProgressUtil.dismissDialog();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, Api.GET_DEVICES);
    }





    private void getNotifications(){
        final String token = "Bearer "+ AppSettings.getUser(getContext()).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_LIMIT, String.valueOf(2));

        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_MULTIPLE_NOTIFICATION
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ProgressUtil.dismissDialog();
                try {
                    for(Device device: mDevices){
                        if(response.has(device.getSerialNumber())){

                            DeviceNotification deviceNotification = new Gson().fromJson(response.getJSONObject(device.getSerialNumber()).toString()
                                    ,DeviceNotification.class);
                            mDeviceNotifications.add(deviceNotification);

                        }
                    }

                    if(mDevices.size() == mDeviceNotifications.size()){
                        for(int i = 0; i<mDeviceNotifications.size();i++){
                            addCard(mDevices.get(i), mDeviceNotifications.get(i).getNotifications());
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    ProgressUtil.dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ProgressUtil.dismissDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(customRequest, Api.GET_MULTIPLE_NOTIFICATION);

    }


   /* private void getAllNotification(Device device){

        String token = AppSettings.getUser(getContext()).getToken();
        Map<String,String> params = new HashMap<>();
        params.put(ApiParams.KEY_TOKEN,token);
        params.put(ApiParams.KEY_LIMIT,String.valueOf(2));
        params.put(ApiParams.KEY_SERIAL_NUMBER,device.getSerialNumber());

        Log.e(TAG, "params : " + params.toString());
        CustomRequest customRequest = new CustomRequest(Request.Method.POST
                , BuildConfig.BASE_URL + Api.GET_NOTIFICATION
                , params
                , new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                DeviceNotification deviceDetail = new Gson().fromJson(response.toString(), DeviceNotification.class);
               *//* mDeviceDetails.add(deviceDetail);
                if(mDevices.size() == mDeviceDetails.size()){
                    for(int i = 0; i<mDeviceDetails.size();i++){
                        addCard(mDevices.get(i), mDeviceDetails.get(i).getNotifications());
                    }
                }*//*

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(customRequest,Api.GET_NOTIFICATION);
    }*/



    public HomeListViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_list_view, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mContainer=(ViewGroup)view.findViewById(R.id.container);
        getAllDevices();
        super.onViewCreated(view, savedInstanceState);
    }
}
