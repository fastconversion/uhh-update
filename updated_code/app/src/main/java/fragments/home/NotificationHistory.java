package fragments.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.GsonBuilder;
import com.uhoo.www.uhoo.AppController;
import com.uhoo.www.uhoo.BuildConfig;
import com.uhoo.www.uhoo.HomeActivity;
import com.uhoo.www.uhoo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.NotificationAdapter;
import adapters.NotificationHistoryAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.Notification;
import parsermodel.NotificationHistoryItem;
import utils.AppSettings;
import utils.NetUtil;
import utils.ProgressUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationHistory extends Fragment {

    private static final String TAG = NotificationHistory.class.getSimpleName();
    ListView listView;
    TextView deviceName;
    NotificationHistoryAdapter notificationAdapter;
    List<NotificationHistoryItem> notificationList;

  Notification notification=null;




    public NotificationHistory() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);








    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_notificationhistory, container, false);
        if(NetUtil.raiseWarning(getActivity(),"Please check network connection and try again")) {
            init(view);
        }

        return view;
    }

    void init(View view)
    {   notification= getArguments().getParcelable(ApiParams.KEY_NOTIFICATION);
        int typeid=notification.getTypeId();


        listView=(ListView)view.findViewById(R.id.listViewnotifications);
        notificationList=new ArrayList<NotificationHistoryItem>();
        notificationAdapter=new NotificationHistoryAdapter(getActivity(),notificationList,typeid);
        listView.setAdapter(notificationAdapter);
        deviceName=(AppCompatTextView)view.findViewById(R.id.textdevicename);
        deviceName.setText(notification.getDeviceName());


        callNotifications();



    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }
    private void callNotifications(){

        final String token = "Bearer "+ AppSettings.getUser(getActivity()).getToken();
        Log.d("Token in Header",token);
        ProgressUtil.showProgressDialog(getActivity(), "Loading", "Please Wait");

        StringRequest stringRequest = new StringRequest(Request.Method.POST,BuildConfig.BASE_URL+ Api.GET_NOTIFICATON_DETAIL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response Notifi Detail", response.toString());

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray notificationsArray=jsonObject.getJSONArray(ApiParams.KEY_NOTIFICATIONS);
                    if(notificationsArray.length()==0)
                    {
                        Toast.makeText(getActivity(),"Sorry is Not Notification in Response",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (int i = 0; i < notificationsArray.length(); i++) {
                            JSONObject singleNotification = notificationsArray.getJSONObject(i);

                            NotificationHistoryItem notification=new NotificationHistoryItem(singleNotification.getString(ApiParams.KEY_MESSAGE),
                                    singleNotification.getInt(ApiParams.KEY_COUNTER),singleNotification.getInt(ApiParams.KEY_LEVEL)

                            );
                            Log.d(ApiParams.KEY_MESSAGE,notification.getMessage());
                            Log.d(ApiParams.KEY_COUNTER,String.valueOf(notification.getCounter()));
                            Log.d(ApiParams.KEY_LEVEL,String.valueOf(notification.getLevel()));
                            notificationList.add(notification);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notificationAdapter.notifyDataSetChanged();
                ProgressUtil.dismissDialog();



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error dummy " + error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new HashMap<>();
                Log.d("Notifi Id in Param",notification.getId());
                params.put(ApiParams.KEY_ID,notification.getId());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_UNCLAIMED);


    }





}
