package fragments.home;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.DeviceDetailActivity;
import com.uhoo.www.uhoo.R;

import java.util.List;

import adapters.CardAdapter;
import parsermodel.Device;
import parsermodel.Notification;
import utils.MyLinearLayoutManager;
import widget.RecyclerView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationViewItemFragment extends Fragment {

    private static final String TAG = NotificationViewItemFragment.class.getSimpleName();
    private Device mDevice;
    private List<Notification> mNotifications;
    private ViewGroup mContainer;

    private void addCard(final Device device, List<Notification> notifications){

        final MyLinearLayoutManager linearLayoutManager = new MyLinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false);

        final ViewGroup notificationCard = (ViewGroup) LayoutInflater.from(getContext()).inflate(
                R.layout.card_view_item, mContainer, false);

        final AppCompatTextView txtTitle = (AppCompatTextView)notificationCard.findViewById(R.id.txt_device_name);
        txtTitle.setText(device.getDeviceName());

        final RecyclerView recyclerView = (RecyclerView)notificationCard.findViewById(R.id.recycler_Card);
        recyclerView.setLayoutManager(linearLayoutManager);

        final CardAdapter cardAdapter = new CardAdapter(getContext(),notifications);
        recyclerView.setAdapter(cardAdapter);

        notificationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), DeviceDetailActivity.class);
//                intent.putExtra("serialNumber", device.getSerialNumber());
//                intent.putExtra("deviceName",device.getDeviceName());
//                getActivity().startActivity(intent);
            }
        });
        mContainer.addView(notificationCard);
    }


    public NotificationViewItemFragment() {
        // Required empty public constructor
    }
    @SuppressLint("ValidFragment")
    public NotificationViewItemFragment(Device device, List<Notification> notifications) {
        mDevice = device;
        this.mNotifications = notifications;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try{
            return inflater.inflate(R.layout.fragment_notification_view_item, container, false);
        }catch(Exception e){
            return null;
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mContainer=(ViewGroup)view.findViewById(R.id.container);
        addCard(mDevice,mNotifications);
        super.onViewCreated(view, savedInstanceState);
    }
}
