package fragments.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uhoo.www.uhoo.AppController;
import com.uhoo.www.uhoo.BuildConfig;
import com.uhoo.www.uhoo.HistoryActivity;
import com.uhoo.www.uhoo.HomeActivity;
import com.uhoo.www.uhoo.R;
import com.uhoo.www.uhoo.SplashActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapters.NotificationAdapter;
import api.Api;
import api.ApiParams;
import parsermodel.Notification;
import utils.AppSettings;
import utils.NetUtil;
import utils.ProgressUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = NotificationFragment.class.getSimpleName();
    ListView listView;
    NotificationAdapter notificationAdapter;
    List<Notification> notificationList;
    SwipeRefreshLayout swipeRefreshLayout;
    HomeActivity homeActivity;




    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationList=new ArrayList<Notification>();
        notificationAdapter=new NotificationAdapter(getActivity(),notificationList);
        homeActivity=new HomeActivity();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_notification, container, false);
        if(NetUtil.raiseWarning(getActivity(),"Please check network connection and try again")) {
            init(view);
        }

        return view;
    }

    void init(View view)
    {
        listView=(ListView)view.findViewById(R.id.listViewnotifications);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d("I am Clicked", String.valueOf(position));
               Notification notification=notificationList.get(position);
                Intent intent=new Intent(getActivity(), HistoryActivity.class);
                intent.putExtra(ApiParams.KEY_NOTIFICATION,notification);
                startActivity(intent);



            }

        });
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        listView.setAdapter(notificationAdapter);

        callNotifications();


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callNotifications();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
    }
    private void callNotifications(){

        final String token = "Bearer "+ AppSettings.getUser(getActivity()).getToken();
        ProgressUtil.showProgressDialog(getActivity(),"Loading","Please Wait");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL+ Api.GET_NOTIFICATION
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Notification Response", response.toString());

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray notificationsArray=jsonObject.getJSONArray(ApiParams.KEY_NOTIFICATIONS);
                    if(notificationsArray.length()==0)
                    {
                        Toast.makeText(getActivity(),"Sorry is Not Notification in Response",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (int i = 0; i < notificationsArray.length(); i++) {
                            JSONObject singleNotification = notificationsArray.getJSONObject(i);
                            Notification notification = new Notification(singleNotification.getString(ApiParams.KEY_ID),
                                    singleNotification.getString(ApiParams.KEY_MESSAGE),
                                    singleNotification.getInt(ApiParams.KEY_LEVEL),
                                    singleNotification.getInt(ApiParams.KEY_COUNTER),
                                    singleNotification.getString(ApiParams.KEY_DEVICE_ID),
                                    singleNotification.getLong(ApiParams.KEY_UPDATE_AT),
                                    singleNotification.getInt(ApiParams.KEY_TYPE_ID),
                                    singleNotification.getInt(ApiParams.KEY_READ), singleNotification.getString(ApiParams.KEY_DEVICE_NAME)
                            );
                            notificationList.add(notification);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notificationAdapter.notifyDataSetChanged();
                ProgressUtil.dismissDialog();



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error dummy " + error.getMessage());
                ProgressUtil.dismissDialog();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(ApiParams.KEY_LIMIT,"10");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_UNCLAIMED);

    }
    private void callExtraNotifications(){
        swipeRefreshLayout.setRefreshing(true);
        final String token = "Bearer "+ AppSettings.getUser(getActivity()).getToken();
        final Notification notification=notificationList.get(notificationList.size()-1);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BuildConfig.BASE_URL + Api.HOME_LIST
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Extra  Response",response.toString());

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    JSONArray notificationsArray=jsonObject.getJSONArray(ApiParams.KEY_NOTIFICATIONS);
                    if(notificationsArray.length()==0)
                    {
                        Toast.makeText(getActivity(),"Sorry,No Notification in Response",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (int i = 0; i < notificationsArray.length(); i++) {
                            JSONObject singleNotification = notificationsArray.getJSONObject(i);
                            Notification notification = new Notification(singleNotification.getString(ApiParams.KEY_ID),
                                    singleNotification.getString(ApiParams.KEY_MESSAGE),
                                    singleNotification.getInt(ApiParams.KEY_LEVEL),
                                    singleNotification.getInt(ApiParams.KEY_COUNTER),
                                    singleNotification.getString(ApiParams.KEY_DEVICE_ID),
                                    singleNotification.getLong(ApiParams.KEY_UPDATE_AT),
                                    singleNotification.getInt(ApiParams.KEY_TYPE_ID),
                                    singleNotification.getInt(ApiParams.KEY_READ), singleNotification.getString(ApiParams.KEY_DEVICE_NAME)
                            );
                            notificationList.add(notification);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notificationAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error dummy " + error.getMessage());
                swipeRefreshLayout.setRefreshing(false);

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put(ApiParams.KEY_LIMIT,"10");
                params.put(ApiParams.KEY_ORDER,"1");
                params.put(ApiParams.KEY_DATE,String.valueOf(notification.getUpdatedAt()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put(ApiParams.KEY_AUTHORIZATION,token);
                return headers;
            }
        };


        AppController.getInstance().addToRequestQueue(stringRequest, Api.DEVICE_UNCLAIMED);

    }


    protected void setUI(Fragment fragment){
        FragmentManager mFragmentManager = getChildFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container,fragment,fragment.getClass().getCanonicalName());
        mFragmentTransaction.commit();
    }

    @Override
    public void onRefresh() {

        if(NetUtil.raiseWarning(getActivity(),"Please check network connection and try again")) {
            callExtraNotifications();
        }

    }
}
