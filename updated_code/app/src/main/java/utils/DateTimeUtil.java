package utils;

import android.content.Context;

import com.uhoo.www.uhoo.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by James on 23-09-2015.
 */
public class DateTimeUtil {

    private static final SimpleDateFormat DATE_AND_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault());
    private static final SimpleDateFormat READABLE_DAY_MONTH_FORMAT = new SimpleDateFormat("d MMMM", Locale.getDefault());
    private static final SimpleDateFormat READABLE_DAY_MONTH_YEAR_FORMAT = new SimpleDateFormat("d MMMM yyyy", Locale.getDefault());
    private static final SimpleDateFormat READABLE_TIME_FORMAT = new SimpleDateFormat("hh:mm a", Locale.getDefault());
    private static final SimpleDateFormat READABLE_DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HHmm", Locale.getDefault());
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
   private static final  DateFormat DATEFORMAT = new SimpleDateFormat("MMM.dd.yyyy.hh:mm:ss aaa", Locale.getDefault());


    public static String getFormattedTime(Calendar calendar){

        return TIME_FORMAT.format(calendar.getTime());
    }
    public static String getFormatedDateWithDayDateYear(Calendar calendar)
    {
        return DATEFORMAT.format(calendar.getTimeInMillis());
    }

    public static String getFormattedDate(Calendar calendar){

        return DATE_FORMAT.format(calendar.getTime());
    }

    public static String getRedableTime(Calendar calendar){

        return READABLE_TIME_FORMAT.format(calendar.getTime());
    }

    public static String getRedableDate(Calendar calendar) {

        return READABLE_DATE_FORMAT.format(calendar.getTime());
    }


    public static Long getLongDateAndTime(Calendar calendar) {

        return Long.parseLong(DATE_AND_TIME_FORMAT.format(calendar.getTime()));
    }

    public static String getDAteAndTime(Calendar calendar) {

        return DATE_AND_TIME_FORMAT.format(calendar.getTime());
    }

    public static String getAppropriateDateFormat(Context context, Calendar calendar) {
        if (isThisYear(calendar)) {
            if (isThisMonth(calendar) && isThisDayOfMonth(calendar)) {
                return context.getResources().getString(R.string.date_today);
            } else {
                return READABLE_DAY_MONTH_FORMAT.format(calendar.getTime());
            }
        } else {
            return READABLE_DAY_MONTH_YEAR_FORMAT.format(calendar.getTime());
        }
    }



    public static Calendar parseDateAndTime(String dateAndTime) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(DATE_AND_TIME_FORMAT.parse(dateAndTime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar;
    }

    private static Boolean isThisYear(Calendar calendar) {
        Calendar nowCalendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR) == nowCalendar.get(Calendar.YEAR);
    }

    private static Boolean isThisMonth(Calendar calendar) {
        Calendar nowCalendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) == nowCalendar.get(Calendar.MONTH);
    }

    private static Boolean isThisDayOfMonth(Calendar calendar) {
        Calendar nowCalendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH) == nowCalendar.get(Calendar.DAY_OF_MONTH);
    }

    public static String getTimeDifference(long time){

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        Date notifiDate = calendar.getTime();
        Date curDate = Calendar.getInstance().getTime();
        long difference = curDate.getTime() - notifiDate.getTime();
        int days = (int) (difference / (1000*60*60*24));
        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
        hours = (hours < 0 ? -hours : hours);
        min = (min<0 ? -min : min);
        days = (days <0 ? -days : days);

        if(hours>24){
            return days+" days ago";
        }else if(hours>0 && hours<24){
            return hours+" hours ago";
        }else {
            return min+" minutes ago";
        }

    }
}
