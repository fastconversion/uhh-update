package utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * Created by James on 14-12-2015.
 */
public class ErrorUtil {
    private static final String TAG = ErrorUtil.class.getSimpleName();

    public static  void alert(Context context,String title,String message){

        try {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(context);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Alert dialog crash");
        }
    }
}
