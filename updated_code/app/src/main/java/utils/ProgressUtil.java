package utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

/**
 * Created by James on 14-12-2015.
 */
public class ProgressUtil {

    private static final String TAG = ProgressUtil.class.getSimpleName();
    private static ProgressDialog mProgressDialog;

    public static void showProgressDialog(Context context,String title,String message){
        dismissDialog();
        mProgressDialog = ProgressDialog.show(context,title,message,true,false);
    }

    public static void dismissDialog(){
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Progress bar crash");
        }
    }
}
