package utils;

import android.content.Context;

import parsermodel.User;

/**
 * Created by James on 10-11-2015.
 */
public class AppSettings {

    public static final String KEY_UHOO= "uhoo";
    public static final String KEY_TOKEN= "token";
    public static final String KEY_REFRESH_TOKEN = "refreshToken";
    public static final String KEY_STATUS = "status";

    public static  void setUser(Context context,User user){
        PrefUtil.putString(context,KEY_UHOO,KEY_TOKEN,user.getToken());
        PrefUtil.putString(context,KEY_UHOO,KEY_REFRESH_TOKEN,user.getRefreshToken());
        PrefUtil.putInt(context, KEY_UHOO, KEY_STATUS, user.getStatus());
    }

    public static User getUser(Context context){
        User user = new User();
        user.setStatus(PrefUtil.getInt(context, KEY_UHOO, KEY_STATUS));
        user.setToken(PrefUtil.getString(context, KEY_UHOO, KEY_TOKEN));
        user.setRefreshToken(PrefUtil.getString(context, KEY_UHOO, KEY_REFRESH_TOKEN));
        return user;
    }

    public static void logoutUser(Context context){
        PrefUtil.clear(context,KEY_UHOO);
    }
}
