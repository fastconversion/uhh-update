package adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.R;

import parsermodel.DeviceDetail;
import widget.RecyclerView;

/**
 * Created by James on 14-11-2015.
 */
public class DeviceDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DeviceDetailAdapter.class.getSimpleName();
    private Context mContext;
    private DeviceDetail mDeviceDetail;



    private int getColor(String colorName){
        if(colorName.equals("yellow")){
            return R.color.yellow;
        }else if(colorName.equals("green")){
            return R.color.green;
        }else if(colorName.equals("red")){
            return R.color.red;
        }
        return 0;
    }

    public Drawable getTintedDrawable(Resources res,
                                      @DrawableRes int drawableResId, @ColorRes int colorResId) {
        Drawable drawable = res.getDrawable(drawableResId);
        int color = res.getColor(colorResId);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public static class DeviceDetailItem extends RecyclerView.ViewHolder{

        public AppCompatTextView txtSensorName,txtUnit,txtValue;
        public AppCompatImageView imgSensor;


        public DeviceDetailItem(View itemView) {
            super(itemView);

            txtSensorName=(AppCompatTextView)itemView.findViewById(R.id.txt_sensor);
            txtUnit=(AppCompatTextView)itemView.findViewById(R.id.txt_unit);
            txtValue=(AppCompatTextView)itemView.findViewById(R.id.txt_value);
            imgSensor=(AppCompatImageView)itemView.findViewById(R.id.img_sensor);


        }
    }

    public DeviceDetailAdapter(Context context, DeviceDetail summary) {
        mContext = context;
        mDeviceDetail = summary;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_detail_item, parent, false);
        return new DeviceDetailItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        final DeviceDetailItem summaryItemView = (DeviceDetailItem)holder;
        switch (position){

            case 0:
                summaryItemView.txtSensorName.setText("TEMPERATURE");
                summaryItemView.txtUnit.setText("CELCIUS");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getTemp().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getTemp().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_temp_white
                        , getColor(mDeviceDetail.getTemp().getColor())));
                break;
            case 1:
                summaryItemView.txtSensorName.setText("HUMIDITY");
                summaryItemView.txtUnit.setText("PERCENT");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getHumidity().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getHumidity().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_humidity_white
                        , getColor(mDeviceDetail.getHumidity().getColor())));
                break;
            case 2:
                summaryItemView.txtSensorName.setText("CARBON DIOXIDE");
                summaryItemView.txtUnit.setText("PPM");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getCo2().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getCo2().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_cdioxide_white
                        , getColor(mDeviceDetail.getCo2().getColor())));
                break;
            case 3:
                summaryItemView.txtSensorName.setText("VOC");
                summaryItemView.txtUnit.setText("PPM");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getVoc().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getVoc().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_voc_white
                        , getColor(mDeviceDetail.getVoc().getColor())));
                break;
            case 4:
                summaryItemView.txtSensorName.setText("PM2.5");
                summaryItemView.txtUnit.setText("ug/m3");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getDust().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getDust().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_pm25_white
                        , getColor(mDeviceDetail.getDust().getColor())));
                break;
            case 5:
                summaryItemView.txtSensorName.setText("AIR PRESSURE");
                summaryItemView.txtUnit.setText("PSI");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getPressure().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getPressure().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_airpressure_white
                        , getColor(mDeviceDetail.getPressure().getColor())));
                break;
            case 6:
                summaryItemView.txtSensorName.setText("CARBON MONOXIDE");
                summaryItemView.txtUnit.setText("PPM");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getCo().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getCo().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_cmonoxide_white
                        , getColor(mDeviceDetail.getCo().getColor())));
                break;
            case 7:
                summaryItemView.txtSensorName.setText("OZONE");
                summaryItemView.txtUnit.setText("PERCENT");
                summaryItemView.txtValue.setText(String.valueOf(mDeviceDetail.getOzone().getValue()));
                summaryItemView.txtValue.setTextColor(mContext.getResources().getColor(getColor(mDeviceDetail.getOzone().getColor())));
                summaryItemView.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                        , R.drawable.sensor_ozone_white
                        , getColor(mDeviceDetail.getOzone().getColor())));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return 8;
    }
}
