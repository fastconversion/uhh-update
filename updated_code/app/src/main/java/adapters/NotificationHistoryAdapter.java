package adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.uhoo.www.uhoo.R;

import java.util.List;

import parsermodel.Notification;
import parsermodel.NotificationHistoryItem;
import utils.DateTimeUtil;


public class NotificationHistoryAdapter extends BaseAdapter{

    Activity activity;
    List<NotificationHistoryItem> notificationList;
    LayoutInflater inflater;
    int  typeId;

   public NotificationHistoryAdapter(Activity activity, List<NotificationHistoryItem> notificationList,int type)
    {
        this.activity=activity;
        this.notificationList=notificationList;
        this.typeId=type;
        Log.d("Type id",String.valueOf(type));

    }
    public NotificationHistoryAdapter(Activity activity, List<NotificationHistoryItem> notificationList)
    {
        this.activity=activity;
        this.notificationList=notificationList;

    }

    private int getImage(int type,int lavel){

        switch (type){
            case 1:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_temp_red;
                    case 2:
                        return R.drawable.sensor_temp_yellow;
                    case 3:
                        return R.drawable.sensor_temp_green;

                }


            case 2:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_humidity_red;
                    case 2:
                        return R.drawable.sensor_humidity_yellow;
                    case 3:
                        return R.drawable.sensor_humidity_green;

                }
            case 3:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_cdioxide_red;
                    case 2:
                        return R.drawable.sensor_cdioxide_yellow;
                    case 3:
                        return R.drawable.sensor_cdioxide_green;

                }
            case 4:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_voc_red;
                    case 2:
                        return R.drawable.sensor_voc_yellow;
                    case 3:
                        return R.drawable.sensor_voc_green;

                }

            case 5:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_pm25_red;
                    case 2:
                        return R.drawable.sensor_pm25_yellow;
                    case 3:
                        return R.drawable.sensor_pm25_green;

                }

            case 6:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_airpressure_red;
                    case 2:
                        return R.drawable.sensor_airpressure_yellow;
                    case 3:
                        return R.drawable.sensor_airpressure_green;

                }

            case 7:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_cmonoxide_red;
                    case 2:
                        return R.drawable.sensor_cmonoxide_yellow;
                    case 3:
                        return R.drawable.sensor_cmonoxide_green;

                }

            case 8:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_ethanol_red;
                    case 2:
                        return R.drawable.sensor_ethanol_yellow;
                    case 3:
                        return R.drawable.sensor_ethanol_green;

                }

            case 9:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_ozone_red;
                    case 2:
                        return R.drawable.sensor_ozone_yellow;
                    case 3:
                        return R.drawable.sensor_ozone_green;

                }
                 default:
                return R.drawable.sensor_temp_white;
        }
    }




    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NotificationHistoryItem notification=notificationList.get(position);

        if(inflater==null)
        {
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.fragment_notificationhistory_view_item,null);

        }

        NotificationHistoryItemDetail notificationDetailItem=new NotificationHistoryItemDetail(convertView);
        notificationDetailItem.txtmessage.setText(notification.getMessage());
        notificationDetailItem.imgSensor.setImageResource(getImage(typeId, notification.getLevel()));

        if(notification.getCounter()==0){
            notificationDetailItem.txtcounter.setText("Counter");

        }else {
            notificationDetailItem.txtcounter.setText(String.valueOf(notification.getCounter()+" minutes"));
        }



        return convertView;
    }

    public static class NotificationHistoryItemDetail {

        public AppCompatTextView txtmessage,txtcounter;
        public AppCompatImageView imgSensor;


        public NotificationHistoryItemDetail(View itemView) {


            txtcounter=(AppCompatTextView)itemView.findViewById(R.id.notificationupdate);
            txtmessage=(AppCompatTextView)itemView.findViewById(R.id.notificationtitle);
            imgSensor=(AppCompatImageView)itemView.findViewById(R.id.notificationicon);



        }
    }
}
