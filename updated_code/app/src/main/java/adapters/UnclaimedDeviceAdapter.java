package adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.R;

import java.util.List;

import parsermodel.UncalimedDevice;
import widget.RecyclerView;

/**
 * Created by James on 16-11-2015.
 */
public class UnclaimedDeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CardAdapter.class.getSimpleName();
    private Context mContext;
    private List<UncalimedDevice> mUncalimedDevices;
    private UnclaimedDeviceListener mUnclaimedDeviceListener;

    public interface UnclaimedDeviceListener{
        public void onDeviceSelected(UncalimedDevice uncalimedDevice);
    }


    public void setUnclaimedDeviceListener(UnclaimedDeviceListener unclaimedDeviceListener) {
        mUnclaimedDeviceListener = unclaimedDeviceListener;
    }

    public static class UnclaimedDeviceItem extends RecyclerView.ViewHolder{
        public AppCompatTextView txtDeviceName;

        public UnclaimedDeviceItem(View itemView) {
            super(itemView);
            txtDeviceName=(AppCompatTextView)itemView.findViewById(R.id.txt_device_name);
        }
    }


    public UnclaimedDeviceAdapter(Context context, List<UncalimedDevice> uncalimedDevices) {
        mContext = context;
        mUncalimedDevices = uncalimedDevices;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.unclaimed_device_item, parent, false);
        return new UnclaimedDeviceItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final UnclaimedDeviceItem listItem = (UnclaimedDeviceItem)holder;
        final UncalimedDevice notification =  mUncalimedDevices.get(position);

        listItem.txtDeviceName.setText(notification.getSerialNumber());

        listItem.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mUnclaimedDeviceListener!=null){
                    mUnclaimedDeviceListener.onDeviceSelected(notification);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUncalimedDevices.size();
    }
}
