package adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.uhoo.www.uhoo.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import parsermodel.Notification;
import utils.DateTimeUtil;


public class NotificationAdapter extends BaseAdapter{

    Activity activity;
    List<Notification> notificationList;
    LayoutInflater inflater;

   public NotificationAdapter(Activity activity, List<Notification> notificationList)
    {
        this.activity=activity;
        this.notificationList=notificationList;

    }

    private int getImage(Notification notification){
        int lavel=notification.getLevel();
        switch (notification.getTypeId()){
            case 1:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_temp_red;
                    case 2:
                        return R.drawable.sensor_temp_yellow;
                    case 3:
                        return R.drawable.sensor_temp_green;

                }


            case 2:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_humidity_red;
                    case 2:
                        return R.drawable.sensor_humidity_yellow;
                    case 3:
                        return R.drawable.sensor_humidity_green;

                }
            case 3:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_cdioxide_red;
                    case 2:
                        return R.drawable.sensor_cdioxide_yellow;
                    case 3:
                        return R.drawable.sensor_cdioxide_green;

                }
            case 4:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_voc_red;
                    case 2:
                        return R.drawable.sensor_voc_yellow;
                    case 3:
                        return R.drawable.sensor_voc_green;

                }

            case 5:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_pm25_red;
                    case 2:
                        return R.drawable.sensor_pm25_yellow;
                    case 3:
                        return R.drawable.sensor_pm25_green;

                }

            case 6:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_airpressure_red;
                    case 2:
                        return R.drawable.sensor_airpressure_yellow;
                    case 3:
                        return R.drawable.sensor_airpressure_green;

                }

            case 7:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_cmonoxide_red;
                    case 2:
                        return R.drawable.sensor_cmonoxide_yellow;
                    case 3:
                        return R.drawable.sensor_cmonoxide_green;

                }

            case 8:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_ethanol_red;
                    case 2:
                        return R.drawable.sensor_ethanol_yellow;
                    case 3:
                        return R.drawable.sensor_ethanol_green;

                }

            case 9:
                switch(lavel)
                {
                    case 1:
                        return R.drawable.sensor_ozone_red;
                    case 2:
                        return R.drawable.sensor_ozone_yellow;
                    case 3:
                        return R.drawable.sensor_ozone_green;

                }

            case 98:

                return R.drawable.iconshared;

            case 99:
                return R.drawable.iconshared;


            default:
                return R.drawable.sensor_temp_white;
        }
    }




    @Override
    public int getCount() {
        return notificationList.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Notification notification=notificationList.get(position);

        if(inflater==null)
        {
            inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.fragment_notification_view_item,null);

        }

        NotificationDetailItem notificationDetailItem=new NotificationDetailItem(convertView);
        if(notification.getDeviceName().equals(" "))
        {
            notificationDetailItem.txtSensorName.setVisibility(View.GONE);
        }
        else {
            notificationDetailItem.txtSensorName.setText(notification.getDeviceName());
        }
        notificationDetailItem.imgSensor.setImageResource(getImage(notification));

        if(notification.getUpdatedAt()==0){
            notificationDetailItem.txtupdate.setVisibility(View.GONE);

        }else {
            Calendar calendar= GregorianCalendar.getInstance();
            calendar.setTimeInMillis(notification.getUpdatedAt());
            notificationDetailItem.txtupdate.setText(DateTimeUtil.getFormatedDateWithDayDateYear(calendar));
        }

        if(notification.getDescription()==null)
        {
            notificationDetailItem.txtdescription.setVisibility(View.GONE);
        }
        else
        {
            notificationDetailItem.txtdescription.setText(notification.getDescription());

        }
        if(notification.getRead()==0)
        {
            notificationDetailItem.myLayout.setBackgroundResource(R.color.unread_notificationitem_background);
        }
        else
        {
            notificationDetailItem.myLayout.setBackgroundResource(R.color.cardview_light_background);
        }
        if(notification.getMessage().equals(" "))
        {
            notificationDetailItem.txtmessage.setVisibility(View.GONE);
        }
        else {
            notificationDetailItem.txtmessage.setText(notification.getMessage());
        }
        if(notification.getTypeId()==99)
        {
            List<String> list = new ArrayList<String>(Arrays.asList(notification.getMessage().split(" , ")));
            String customMessage=list.get(1)+"is being shared from"+list.get(0);
            notificationDetailItem.txtmessage.setText(customMessage);
            notificationDetailItem.btn.setVisibility(View.VISIBLE);

        }
        return convertView;
    }

    public static class NotificationDetailItem {

        public AppCompatTextView txtSensorName,txtupdate,txtmessage,txtdescription;
        public AppCompatImageView imgSensor;
        AppCompatButton btn;
        RelativeLayout myLayout;


        public NotificationDetailItem(View itemView) {

            myLayout=(RelativeLayout)itemView.findViewById(R.id.container);
            txtSensorName=(AppCompatTextView)itemView.findViewById(R.id.notificationtitle);
            txtupdate=(AppCompatTextView)itemView.findViewById(R.id.notificationupdate);
            txtmessage=(AppCompatTextView)itemView.findViewById(R.id.notificationmessage);
            imgSensor=(AppCompatImageView)itemView.findViewById(R.id.notificationicon);
            txtdescription=(AppCompatTextView)itemView.findViewById(R.id.notificationdescription);
            btn=(AppCompatButton)itemView.findViewById(R.id.btnaccept);


        }
    }
}
