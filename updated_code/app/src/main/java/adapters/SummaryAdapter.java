package adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.R;

import java.util.List;

import parsermodel.Device;
import parsermodel.DeviceDetail;
import widget.RecyclerView;

/**
 * Created by James on 14-11-2015.
 */
public class SummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SummaryAdapter.class.getSimpleName();
    private Context mContext;
    private List<Device> mDevices;
    private List<DeviceDetail> mSummaries;
    private SummaryItemClickListener mSummaryItemClickListener;

    public interface SummaryItemClickListener{
        void onSummaryItemClicked(Device device);
    }


    public void setSummaryItemClickListener(SummaryItemClickListener summaryItemClickListener) {
        mSummaryItemClickListener = summaryItemClickListener;
    }

    private int getColor(String colorName){
        if(colorName.equals("yellow")){
            return R.color.yellow;
        }else if(colorName.equals("green")){
            return R.color.green;
        }else if(colorName.equals("red")){
            return R.color.red;
        }
        return 0;
    }


    public static class SummaryItemView extends RecyclerView.ViewHolder{

        public AppCompatTextView txtDeviceName;
        public AppCompatImageView imgTemprature,imgHumidity,imgCdioxide,imgVoc,imgPm25
                ,imgAirPressure,imgCmonoxide,imgEthanol,imgOzone,imgDummy;

        public SummaryItemView(View itemView) {
            super(itemView);

            txtDeviceName=(AppCompatTextView)itemView.findViewById(R.id.txt_device_name);
            imgTemprature=(AppCompatImageView)itemView.findViewById(R.id.img_temperature);
            imgHumidity=(AppCompatImageView)itemView.findViewById(R.id.img_humidity);
            imgCdioxide=(AppCompatImageView)itemView.findViewById(R.id.img_cdioxide);
            imgVoc=(AppCompatImageView)itemView.findViewById(R.id.img_voc);
            imgPm25=(AppCompatImageView)itemView.findViewById(R.id.img_pm25);
            imgAirPressure=(AppCompatImageView)itemView.findViewById(R.id.img_airpressure);
            imgCmonoxide=(AppCompatImageView)itemView.findViewById(R.id.img_cmonoxide);
            imgEthanol=(AppCompatImageView)itemView.findViewById(R.id.img_ethanol);
            imgOzone=(AppCompatImageView)itemView.findViewById(R.id.img_ozone);
            imgDummy=(AppCompatImageView)itemView.findViewById(R.id.img_dummy);

        }
    }

    public SummaryAdapter(Context context, List<Device> devices, List<DeviceDetail> summaries) {
        mContext = context;
        mDevices = devices;
        mSummaries = summaries;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.summary_item, parent, false);
        return new SummaryItemView(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final DeviceDetail summary = mSummaries.get(position);
        final Device device = mDevices.get(position);
        final SummaryItemView summaryItemView = (SummaryItemView)holder;
        summaryItemView.txtDeviceName.setText(device.getDeviceName());
        summaryItemView.imgTemprature.setBackgroundResource(getColor(summary.getTemp().getColor()));
        summaryItemView.imgHumidity.setBackgroundResource(getColor(summary.getHumidity().getColor()));
        summaryItemView.imgCdioxide.setBackgroundResource(getColor(summary.getCo2().getColor()));
        summaryItemView.imgVoc.setBackgroundResource(getColor(summary.getVoc().getColor()));
        summaryItemView.imgPm25.setBackgroundResource(getColor(summary.getDust().getColor()));
        summaryItemView.imgAirPressure.setBackgroundResource(getColor(summary.getPressure().getColor()));
        summaryItemView.imgCmonoxide.setBackgroundResource(getColor(summary.getCo().getColor()));
      //  summaryItemView.imgEthanol.setBackgroundResource(getColor(summary.getEthanol().getColor()));
        summaryItemView.imgOzone.setBackgroundResource(getColor(summary.getOzone().getColor()));
       // summaryItemView.imgDummy.setBackgroundResource(getColor("green"));

        summaryItemView.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mSummaryItemClickListener!=null){
                    mSummaryItemClickListener.onSummaryItemClicked(device);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSummaries.size();
    }
}
