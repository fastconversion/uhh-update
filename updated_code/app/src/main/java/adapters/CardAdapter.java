package adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uhoo.www.uhoo.R;

import java.util.List;

import parsermodel.Notification;
import utils.DateTimeUtil;
import widget.RecyclerView;

/**
 * Created by James on 16-11-2015.
 */
public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CardAdapter.class.getSimpleName();
    private Context mContext;
    private List<Notification> mNotifications;


    private int getImage(int type){

        switch (type){
            case 1:
                return R.drawable.sensor_temp_white;
            case 2:
                return R.drawable.sensor_humidity_white;
            case 3:
                return R.drawable.sensor_cdioxide_white;
            case 4:
                return R.drawable.sensor_voc_white;
            case 5:
                return R.drawable.sensor_pm25_white;
            case 6:
                return R.drawable.sensor_airpressure_white;
            case 7:
                return R.drawable.sensor_cmonoxide_white;
            case 8:
                return R.drawable.sensor_ethanol_white;
            case 9:
                return R.drawable.sensor_ozone_white;
            case 97:
                return R.drawable.sensor_temp_white;

            default:
                return R.drawable.sensor_temp_white;
        }
    }

    private int getColor(int level){

        switch (level){
            case 1:
                return R.color.red;
            case 2:
                return R.color.yellow;
            case 3:
                return R.color.green;
            default:
                return R.color.green;
        }
    }

    public Drawable getTintedDrawable(Resources res,
                                      @DrawableRes int drawableResId, @ColorRes int colorResId) {
        Drawable drawable = res.getDrawable(drawableResId);
        int color = res.getColor(colorResId);
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return drawable;
    }



    public static class ListItem extends RecyclerView.ViewHolder{
        public AppCompatTextView txtMessage,txtTimeStamp;
        public AppCompatImageView imgSensor;
        public ListItem(View itemView) {
            super(itemView);

            txtMessage=(AppCompatTextView)itemView.findViewById(R.id.txt_message);
            imgSensor=(AppCompatImageView)itemView.findViewById(R.id.img_sensor);
            txtTimeStamp=(AppCompatTextView)itemView.findViewById(R.id.txt_timeStamp);
        }


    }


    public CardAdapter(Context context, List<Notification> notifications) {
        mContext = context;
        mNotifications = notifications;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        return new ListItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ListItem listItem = (ListItem)holder;
        final Notification notification =  mNotifications.get(position);
        listItem.txtMessage.setText(notification.getMessage());
        listItem.imgSensor.setBackgroundDrawable(getTintedDrawable(mContext.getResources()
                , getImage(notification.getTypeId())
                , getColor(notification.getLevel())));

       listItem.txtTimeStamp.setText( DateTimeUtil.getTimeDifference(notification.getUpdatedAt()));
    }

    @Override
    public int getItemCount() {
        return mNotifications.size();
    }
}
